$.extend($.fn.dataTableExt.oSort, {
    "numeric-comma-pre": function ( a ) {
        var x = (a == '-') ? 0 : a.replace(/\./, '').replace( /,/, '.');
        return parseFloat(x);
    },

    "numeric-comma-asc": function ( a, b ) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },

    "numeric-comma-desc": function ( a, b ) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
});

$.extend( $.fn.dataTableExt.oSort, {
    "date-euro-pre": function ( a ) {
        if ($.trim(a) != '') {
            var frDatea = $.trim(a).split(' ');
            var frTimea = frDatea[1] != undefined ? frDatea[1].split(':') : [0, 0, 0];
            var frDatea2 = frDatea[0].split('-');
            return (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + frTimea[2]) * 1;
        } else {
            return 10000000000000; // = l'an 1000 ...
        }
    },

    "date-euro-asc": function ( a, b ) {
        return a - b;
    },

    "date-euro-desc": function ( a, b ) {
        return b - a;
    }
});

// Set custom css classes
$.extend($.fn.DataTable.ext.classes, {
    "sPageButton": "btn btn-kameli",
    "sPaging": 'btn-group dataTables_paginate paging_'
} );

$.extend(true, $.fn.dataTable.defaults, {
    pagingType: 'simple',
    order: [],
    dom: "<'row'r>t<'row'<'col-md-4'l><'col-md-4'i><'col-md-4'p>>",
    lengthMenu: [[5, 10, 20, -1], [5, 10, 20, "Alle"]],
    language: {
        "lengthMenu": "Vis _MENU_ linier per side",
        "zeroRecords":  "Ingen linier matcher søgningen",
        "search": "Søg",
        "info": "Viser _START_ til _END_ af _TOTAL_",
        "infoEmpty":    "Viser 0 linier",
        "infoFiltered": "(filtreret fra _MAX_ linier)",
        "paginate": {
            "previous": "Forrige",
            "next": "Næste"
        }
    }
});