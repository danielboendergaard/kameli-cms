;(function ( $, window, document, undefined ) {

    // Create the defaults once
    var pluginName = "mediamanager",
        defaults = {
            callback: undefined,
            modal: '#media-modal',
            type: 'images'
        };

    // The actual plugin constructor
    function Plugin ( element, options ) {
        this.element = element;
        this.settings = $.extend( {}, defaults, options );
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {
        init: function () {
            var plugin = this,
                dataType = $(this.element).data('type');

            // Find the modal
            this.modal = $(this.settings.modal);

            // Determine the data type
            this.type = dataType ? dataType : this.settings.type;

            // Find the iframe
            this.iframe = this.modal.find('iframe');

            // Add the click listener to the element
            $(this.element).on('click', function(e) {
                e.preventDefault();

                plugin.clickHandler();
            });
        },

        clickHandler: function () {
            var plugin = this;

            // Open the modal
            this.modal.modal();

            // Add the image action when the iframe has loaded
            this.iframe.on('load', function() {
                if (plugin.type == 'files') {
                    plugin.addFileAction();
                } else {
                    plugin.addImageAction();
                }

                plugin.addDeleteAction();
            });

            // Load the iframe if not loaded
            if ( ! this.iframe.attr('src')) {
                this.iframe.attr('src', this.modal.data('url'));
            } else {
                this.iframe.trigger('load');
            }
        },

        addFileAction: function() {
            var plugin = this,
                $items = this.iframe.contents().find('.kameli-manager-item:not(.js-folder)');

            // Pass the id of the image to the callback
            $items.off('click.mediamanager').on('click.mediamanager', function() {
                var $item = $(this);

                if (plugin.settings.callback !== undefined) {
                    plugin.settings.callback($item.data('id'));
                } else {
                    plugin.defaultFileAction($item);
                }

                // Close the modal
                plugin.modal.modal('hide');
            });
        },

        addImageAction: function() {
            var plugin = this,
                $items = this.iframe.contents().find('.kameli-manager-item:not(.js-folder)');

            // Pass the id of the image to the callback
            $items.off('click.mediamanager').on('click.mediamanager', function() {
                var $item = $(this);

                if (plugin.settings.callback !== undefined) {
                    plugin.settings.callback($item.data('id'));
                } else {
                    plugin.defaultAction($item);
                }

                // Close the modal
                if ($(plugin.element).data('content')) plugin.modal.modal('hide');
            });
        },

        addDeleteAction: function() {
            var $items = this.iframe.contents().find('.kameli-manager-delete');

            $items.off('click').on('click', function(e) {
                e.stopPropagation();
                e.preventDefault();

                var $container = $(this).parent();

                if (confirm('Er du sikker på at du vil slette elementet?')) {
                    $.get(this.href, function() {
                        $container.fadeOut(function() {
                            $container.remove();
                        });
                    });
                }
            });
        },

        defaultAction: function($item) {
            var $element = $(this.element),
                imgSrc = $item.find('img').attr('src');

            switch ($element.data('content')) {
                case 'text':
                    imgSrc = imgSrc.replace('tn/', 'large/');

                    CKEDITOR.instances[$element.data('instance')]
                        .insertHtml('<img src="'+ imgSrc +'" class="img-responsive">');

                    break;

                case 'images':
                    var elem = '<div class="kameli-post-image">' +
                        '<img src="' + imgSrc + '" class="img-responsive absolute-center">' +
                        '<input type="hidden" name="' + $element.data('name') + '" value="' + $item.data('id') + '">'+
                        '</div>';

                    $($element.data('target')).append(elem);

                    break;

                case 'image':
                    var $target = $($element.data('target'));

                    $target.val($item.data('id'));

                    $element.siblings('.media-container')
                        .html('<div class="mbm"><img src="'+imgSrc+'" class="img-thumbnail"></div>')
            }
        },

        defaultFileAction: function($item) {
            var $element = $(this.element),
                href = $item.data('url'),
                title = $item.attr('title');

            CKEDITOR.instances[$element.data('instance')].insertHtml('<a href="'+ href +'">' + title + '</a>');
        }
    };

    $.fn[ pluginName ] = function ( options ) {
        return this.each(function() {
            if ( !$.data( this, "plugin_" + pluginName ) ) {
                $.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
            }
        });
    };

    // Data API
    $('[data-toggle="mediamanager"]')[pluginName]();

})( jQuery, window, document );