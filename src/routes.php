<?php
/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
*/

use Cartalyst\Sentry\Users\UserNotFoundException;

Route::group(['prefix' => Config::get('kameli/cms::backend_prefix')], function()
{
    // Auth Routes
    Route::get('log-ind', ['as' => 'cms.login', 'uses' => 'Kameli\Cms\AuthController@getLogin']);
    Route::post('log-ind', 'Kameli\Cms\AuthController@postLogin');
    Route::get('log-ud', ['as' => 'cms.logout', 'uses' => 'Kameli\Cms\AuthController@getLogout']);

    Route::get('bruger/aktiver/{code}', ['as' => 'cms.user.activation', 'uses' => 'Kameli\Cms\UserController@activation']);
    Route::post('bruger/aktiver/{code}', ['as' => 'cms.user.activate', 'uses' => 'Kameli\Cms\UserController@activate']);

    Route::group(['before' => 'auth.cms'], function()
    {
        Route::get('/', ['as' => 'cms.index', 'uses' => 'Kameli\Cms\CmsController@index']);

        Route::model('cms_page', 'Kameli\Cms\Models\Page');

        Route::get('side', ['as' => 'cms.page.index', 'uses' => 'Kameli\Cms\PageController@index']);
        Route::get('side/opret', ['as' => 'cms.page.create', 'uses' => 'Kameli\Cms\PageController@create']);
        Route::post('side', ['as' => 'cms.page.store', 'uses' => 'Kameli\Cms\PageController@store']);

        Route::get('side/{cms_page}/rediger', ['as' => 'cms.page.edit', 'uses' => 'Kameli\Cms\PageController@edit']);
        Route::patch('side/{cms_page}', ['as' => 'cms.page.update', 'uses' => 'Kameli\Cms\PageController@update']);

        Route::get('side/{id}/slet', ['as' => 'cms.page.destroy', 'uses' => 'Kameli\Cms\PageController@destroy']);

        Route::get('nyhed', ['as' => 'cms.news.index', 'uses' => 'Kameli\Cms\NewsController@index']);
        Route::get('nyhed/opret', ['as' => 'cms.news.create', 'uses' => 'Kameli\Cms\NewsController@create']);
        Route::post('nyhed', ['as' => 'cms.news.store', 'uses' => 'Kameli\Cms\NewsController@store']);

        Route::get('nyhed/{id}/rediger', ['as' => 'cms.news.edit', 'uses' => 'Kameli\Cms\NewsController@edit']);
        Route::patch('nyhed/{id}', ['as' => 'cms.news.update', 'uses' => 'Kameli\Cms\NewsController@update']);

        Route::get('nyhed/{id}/slet', ['as' => 'cms.news.destroy', 'uses' => 'Kameli\Cms\NewsController@destroy']);

        Route::model('cms_menu', 'Kameli\Cms\Models\Menu');

        Route::get('menu', ['as' => 'cms.menu.index', 'uses' => 'Kameli\Cms\MenuController@index']);
        Route::get('menu/opret/{cms_menu?}', ['as' => 'cms.menu.create', 'uses' => 'Kameli\Cms\MenuController@create']);
        Route::post('menu', ['as' => 'cms.menu.store', 'uses' => 'Kameli\Cms\MenuController@store']);
        Route::get('menu/{cms_menu}/rediger', ['as' => 'cms.menu.edit', 'uses' => 'Kameli\Cms\MenuController@edit']);
        Route::patch('menu/{cms_menu}', ['as' => 'cms.menu.update', 'uses' => 'Kameli\Cms\MenuController@update']);
        Route::get('menu/{cms_menu}', ['as' => 'cms.menu.show', 'uses' => 'Kameli\Cms\MenuController@show']);
        Route::get('menu/{id}/slet', ['as' => 'cms.menu.destroy', 'uses' => 'Kameli\Cms\MenuController@destroy']);
        Route::post('menu/update-order', ['as' => 'cms.menu.update-order', 'uses' => 'Kameli\Cms\MenuController@updateOrder']);
        Route::post('menu/store-section-status', ['as' => 'cms.menu.store-section-status', 'uses' => 'Kameli\Cms\MenuController@storeSectionStatus']);

        Route::get('kategori', ['as' => 'cms.category.index', 'uses' => 'Kameli\Cms\CategoryController@index']);
        Route::post('kategori', ['as' => 'cms.category.store', 'uses' => 'Kameli\Cms\CategoryController@store']);
        Route::get('kategori/{id}/rediger', ['as' => 'cms.category.edit', 'uses' => 'Kameli\Cms\CategoryController@edit']);
        Route::patch('kategori/{id}', ['as' => 'cms.category.update', 'uses' => 'Kameli\Cms\CategoryController@update']);
        Route::get('kategori/{id}/slet', ['as' => 'cms.category.destroy', 'uses' => 'Kameli\Cms\CategoryController@destroy']);

        Route::get('medie/{id}/slet', ['as' => 'cms.media.destroy', 'uses' => 'Kameli\Cms\MediaController@destroy']);
        Route::get('medie/{dir1?}/{dir2?}/{dir3?}/{dir4?}/{dir5?}', ['as' => 'cms.media.index', 'uses' => 'Kameli\Cms\MediaController@index']);
        Route::post('medie/{dir1?}/{dir2?}/{dir3?}/{dir4?}/{dir5?}', ['as' => 'cms.media.store', 'uses' => 'Kameli\Cms\MediaController@store']);

        Route::get('upload/{id}/slet', ['as' => 'cms.upload.destroy', 'uses' => 'Kameli\Cms\UploadController@destroy']);
        Route::get('upload/{dir1?}/{dir2?}/{dir3?}/{dir4?}/{dir5?}', ['as' => 'cms.upload.index', 'uses' => 'Kameli\Cms\UploadController@index']);
        Route::post('upload/{dir1?}/{dir2?}/{dir3?}/{dir4?}/{dir5?}', ['as' => 'cms.upload.store', 'uses' => 'Kameli\Cms\UploadController@store']);

        Route::get('bruger', ['as' => 'cms.user.index', 'uses' => 'Kameli\Cms\UserController@index']);
        Route::get('bruger/opret', ['as' => 'cms.user.create', 'uses' => 'Kameli\Cms\UserController@create']);
        Route::post('bruger', ['as' => 'cms.user.store', 'uses' => 'Kameli\Cms\UserController@store']);

        Route::get('bruger/ret', ['as' => 'cms.user.edit-user', 'uses' => 'Kameli\Cms\UserController@editCurrentUser']);
        Route::post('bruger/ret', ['as' => 'cms.user.store-user', 'uses' => 'Kameli\Cms\UserController@storeCurrentUser']);

        Route::get('bruger/skift-password', ['as' => 'cms.user.edit-password', 'uses' => 'Kameli\Cms\UserController@editCurrentUserPassword']);
        Route::post('bruger/skift-password', ['as' => 'cms.user.store-password', 'uses' => 'Kameli\Cms\UserController@storeCurrentUserPassword']);

        Route::get('indstillinger', ['as' => 'cms.settings.index', 'uses' => 'Kameli\Cms\SettingsController@index']);
        Route::post('indstillinger', 'Kameli\Cms\SettingsController@update');

        Route::get('bruger/{id}/rediger', ['as' => 'cms.user.edit', 'uses' => 'Kameli\Cms\UserController@edit']);
        Route::patch('bruger/{id}', ['as' => 'cms.user.update', 'uses' => 'Kameli\Cms\UserController@update']);
        Route::get('bruger/{id}/slet', ['as' => 'cms.user.destroy', 'uses' => 'Kameli\Cms\UserController@destroy']);

        if (KameliCms::isMultiLanguage())
        {
            Route::get('sprog', ['as' => 'cms.language.index', 'uses' => 'Kameli\Cms\LanguageController@index']);
            Route::post('sprog', 'Kameli\Cms\LanguageController@update');
        }
    });
});