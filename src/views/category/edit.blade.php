@extends('kameli/cms::layout.base')

@section('content')

<h1 class="kameli-row">Rediger kategori</h1>

<div class="kameli-row">
    <div class="row">

        <div class="col-md-6">
            <div class="kameli-section">
                <h3 class="kameli-section-header">Rediger kategori</h3>

                {{ Form::model($category, ['route' => ['cms.category.update', $category->id], 'method' => 'patch']) }}

                <div class="form-group">
                    {{ Form::label('name', 'Kategori') }}
                    {{ Form::text('name', null, ['class' => 'form-control']) }}
                </div>

                @include('kameli/cms::_partials.form-messages')

                {{ Form::submit('Gem', ['class' => 'btn btn-primary']) }}

                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>

@stop