@extends('kameli/cms::layout.base')

@section('content')

<h1 class="kameli-row">Kategorier</h1>

<div class="kameli-row">
    <div class="row">
        <div class="col-md-6">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Navn</th>
                    <th>Sider</th>
                    <th>Handlinger</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                <tr>
                    <td>{{ $category->name }}</td>
                    <td>{{ $category->publishedPages()->count() }}</td>
                    <td>
                        {{ link_to_route('cms.category.edit', 'Rediger', $category->id, ['class' => 'btn btn-xs btn-default']) }}
                        <div class="btn btn-xs btn-danger js-delete" data-url="{{ route('cms.category.destroy', $category->id) }}">Slet</div>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

            @if(Session::has('error'))
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif

        </div>
        <div class="col-md-6">
            <div class="kameli-section">
                <h3 class="kameli-section-header">Tilføj kategori</h3>

                {{ Form::open() }}

                <div class="form-group">
                    {{ Form::label('name', 'Kategori') }}
                    {{ Form::text('name', null, ['class' => 'form-control']) }}
                </div>

                @include('kameli/cms::_partials.form-messages')

                {{ Form::submit('Gem', ['class' => 'btn btn-primary']) }}

                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript">
    (function() {
        $('.js-delete').on('click', function() {
            var $btn = $(this);

            bootbox.setDefaults({locale: 'da'});
            bootbox.confirm('Er du sikker på at du vil slette kategorien?', function(result) {
                if (result) window.location.href = $btn.data('url');
            });
        });
    })();
</script>
@stop