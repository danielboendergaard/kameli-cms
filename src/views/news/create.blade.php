@extends('kameli/cms::layout.base')

@section('head')
<link rel="stylesheet" href="{{ asset('packages/kameli/cms/bootstrap-timepicker/css/bootstrap-datetimepicker.min.css') }}"/>
@stop

@section('content')
{{ Form::open(['route' => 'cms.news.store']) }}

<div class="kameli-row">
    <h1>Opret nyhed</h1>

    <div class="label label-default label-header-message">Du har ugemte ændringer</div>

    <div class="header-btn-container">
        @foreach($languages as $id => $lang)
        <div class="btn-group">
            <a href="#{{ $lang->code }}" class="btn btn-primary btn-tab {{ $id == 0 ? 'active' : '' }}">
                <span>{{ $lang->name_da }}</span>
            </a>
            <label class="btn btn-primary btn-tab">
                {{ Form::checkbox('', 1, null, ['class' => 'js-published-'.$lang->code, 'data-ays-ignore' => 'true']) }}
            </label>
        </div>
        @endforeach

        {{ Form::submit('Gem', ['name' => '_save-and-stay', 'class' => 'btn btn-default btn-tab']) }}
        {{ Form::submit('Gem og luk', ['class' => 'btn btn-default btn-tab']) }}
    </div>
</div>

@if(Session::has('lang_errors'))
    <div class="alert alert-danger alert-block mtl mhxl mbn">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4>Indtastningsfejl</h4>

        @foreach(Session::get('lang_errors') as $lang => $errors)
        <strong>{{ $lang }}</strong><br>
        {{ implode('', $errors->all(':message<br>')) }}
        @endforeach
    </div>
@endif

<div class="tab-content">
    @foreach($languages as $id => $lang)
    <div id="{{ $lang->code }}" class="tab-pane fade {{ $id == 0 ? 'in active' : '' }}">
        <div class="kameli-row">
            <div class="kameli-section">
                <h4 class="kameli-section-header">Egenskaber</h4>

                <div class="form-group">
                    {{ Form::label($lang->code.'[title]', 'Titel') }}
                    {{ Form::text($lang->code.'[title]', null, ['class' => 'form-control']) }}
                </div>

                <div class="row">
                    <div class="form-group col-lg-4">
                        {{ Form::label($lang->code.'[public]', 'Offentlig') }}
                        {{ Form::select($lang->code.'[public]', [0 => 'Nej', 1 => 'Ja'], $id == 0, ['class' => 'form-control js-published-select']) }}
                    </div>

                    <div class="form-group col-lg-4">
                        {{ Form::label($lang->code.'[publish]', 'Start dato') }}

                        <div class="input-group">
                            <label class="input-group-addon">
                                {{ Form::checkbox($lang->code.'[publish_on]', 1, null, ['class' => 'js-date-activate']) }}
                            </label>
                            {{ Form::text($lang->code.'[publish]', $now, ['class' => 'form-control timepicker']) }}
                        </div>
                    </div>

                    <div class="form-group col-lg-4">
                        {{ Form::label($lang->code.'[unpublish]', 'Slut dato') }}

                        <div class="input-group">
                            <label class="input-group-addon">
                                {{ Form::checkbox($lang->code.'[unpublish_on]', 1, null, ['class' => 'js-date-activate']) }}
                            </label>
                            {{ Form::text($lang->code.'[unpublish]', $now, ['class' => 'form-control timepicker']) }}
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label($lang->code.'[meta_description]', 'META description') }}
                    {{ Form::textarea($lang->code.'[meta_description]', null, ['class' => 'form-control', 'rows' => 2]) }}
                </div>
            </div>
        </div>

        <div class="kameli-row">

            <div class="form-group">
                {{ Form::label($lang->code.'[short_description]', 'Kort beskrivelse') }}
                {{ Form::textarea($lang->code.'[short_description]', null, ['class' => 'form-control', 'rows' => 2]) }}
            </div>

            <div class="form-group">
                {{ Form::label($lang->code.'[content]', 'Indhold') }}
                {{ Form::textarea($lang->code.'[content]', null, ['class' => 'form-control js-ckeditor', 'id' => 'cke-'.$lang->code]) }}

                <button class="btn btn-primary mtm" data-toggle="mediamanager" data-content="text" data-instance="cke-{{ $lang->code }}">Indsæt billede</button>
            </div>
        </div>

    </div>
    @endforeach
</div>

{{ Form::close() }}

@stop

@section('js')
<script type="text/javascript" src="{{ asset('packages/kameli/cms/bootstrap-timepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('packages/kameli/cms/bootstrap-timepicker/js/locales/bootstrap-datetimepicker.da.js') }}"></script>
<script type="text/javascript">
    (function() {

        $('.js-date-activate').on('change', function() {
            var $this = $(this);

            $this.parent().next().attr('disabled', ! $this.is(':checked'));
        }).trigger('change');

        $('.timepicker').datetimepicker({
            weekStart: 1,
            language: 'da',
            format: 'dd-MM-yyyy hh:mm:ss'
        });

        /*
        |--------------------------------------------------------------------------
        | Submit on template change
        |--------------------------------------------------------------------------
        */

        $('.js-submit').on('change', function() {
            $(window).unbind('beforeunload');

            $(this).closest('form').submit();
        });

        /*
        |--------------------------------------------------------------------------
        | Control tab button states
        |--------------------------------------------------------------------------
        */

        $('.btn:first-child').on('click', function() {
            var $this = $(this);

            if ($this.hasClass('active')) return;

            $this.tab('show');

            $this.addClass('active').parent().siblings().find('.btn:first-child').removeClass('active');
        });

        /*
        |--------------------------------------------------------------------------
        | Detect changes in forms
        |--------------------------------------------------------------------------
        */

        var $messageLabel = $('.label-header-message');

        $('form').areYouSure({message: 'Du har ændringer der ikke er gemt', change: function(form) {
            form.hasClass('dirty') ? $messageLabel.fadeIn() : $messageLabel.fadeOut();
        }});

        /*
        |--------------------------------------------------------------------------
        | CKEditor
        |--------------------------------------------------------------------------
        */

        // Instantiate CKEditor
        $('.js-ckeditor').ckeditor();

        // Fire change event on textarea when ckeditor instances register changes
        $.each(CKEDITOR.instances, function(index, item) {
            var $element = $(item.element.$);

            item.on('change', function() {
                $element.change();
            })
        });

        /*
        |--------------------------------------------------------------------------
        | Keep published status in sync
        |--------------------------------------------------------------------------
        */

        $('[class^="js-published"]').on('change', function() {
            var $this = $(this),
                selector = "[name='"+ $this.attr('class').substr(-2)  +"[public]']";

            $(selector).val($this.is(':checked') ? 1 : 0).change();
        });

        $('.js-published-select').on('change', function() {
            var code = $(this).attr('name').substr(0, 2);

            $('.js-published-'+code).attr('checked', 1 == $(this).val());
        }).change();

    })();
</script>
@stop