@extends('kameli/cms::layout.base')

@section('content')
<div class="kameli-row">
    <h1>Nyheder</h1>

    <div class="header-btn-container">
        <a href="{{ route('cms.news.create') }}" class="btn btn-primary btn-tab">Opret ny nyhed</a>
    </div>
</div>

<div class="kameli-row">

    <div class="form-group">
        <input type="text" class="form-control js-filter" placeholder="Søg her">
    </div>

    <table id="dt" class="table table-striped kameli-table">
        <thead>
        <tr>
            <th>Titel</th>
            <th>Offentlige sprog</th>
            <th>Oprettet</th>
            <th>Handlinger</th>
        </tr>
        </thead>
        <tbody>
        @foreach($news as $story)
        <tr>
            <td>{{ $story->title }}</td>
            <td>{{ $story->publishedLanguages()->implode('name_da', ', ') }}</td>
            <td>{{ $story->created_at->format('d-m-Y H:i:s') }}</td>
            <td>
                {{ link_to_route('cms.news.edit', 'Rediger', $story->id, ['class' => 'btn btn-xs btn-default']) }}
                <div class="btn btn-xs btn-danger js-delete" data-url="{{ route('cms.news.destroy', $story->id) }}">Slet</div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    @include('kameli/cms::_partials.form-messages')

    @if (Session::has('related'))
    <div class="alert alert-danger alert-block mtm">
        <p>Nyheden kan ikke slettes da den bliver linket til fra følgende:</p>

        @foreach (Session::get('related') as $type => $items)
        <p>
            <strong>{{ $type }}</strong><br>

            @foreach ($items as $item)
            <span>{{ $item->linkable->getDisplayName() }}</span><br>
            @endforeach
        </p>
        @endforeach
    </div>
    @endif

</div>
@stop

@section('js')
<script src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
<script src="/packages/kameli/cms/js/datatables.defaults.js"></script>
<script type="text/javascript">
    (function() {
        var $table = $('#dt').DataTable({
            displayLength: -1,
            columnDefs: [
                {
                    sortable: false,
                    searchable: false,
                    targets: [-1]
                },
                {
                    type: 'date-euro',
                    targets: [2]
                }
            ]
        });

        // Attach filter event to input
        $('.js-filter').on('keyup', function() {
            $table.fnFilter($(this).val(), 0);
        });

        $('.js-delete').on('click', function() {
            var $btn = $(this);

            bootbox.setDefaults({locale: 'da'});
            bootbox.confirm('Er du sikker på at du vil slette nyheden?', function(result) {
                if (result) window.location.href = $btn.data('url');
            });
        });
    })();
</script>
@stop