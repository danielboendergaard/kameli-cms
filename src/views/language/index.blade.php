@extends('kameli/cms::layout.base')

@section('content')
{{ Form::open() }}

<div class="kameli-row">
    <h1>Sprog</h1>

    <div class="header-btn-container">
        <button type="submit" class="btn btn-primary btn-tab">Gem</button>
    </div>
</div>

<div class="kameli-row">
    <div class="kameli-section">
        <h4 class="kameli-section-header">Tilgængelige sprog</h4>

        @foreach($languages as $language)
        <div class="form-group">
            {{ Form::label("language[$language->id]", $language->name_da) }}
            {{ Form::select("language[$language->id]", [0 => 'Nej', 1 => 'Ja'], $language->active, ['class' => 'form-control', 'disabled' => $language->id == 1 ?: null]) }}
        </div>
        @endforeach

        @include('kameli/cms::_partials.form-messages')
    </div>
</div>

{{ Form::close() }}
@stop