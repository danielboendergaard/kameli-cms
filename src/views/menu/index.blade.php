@extends('kameli/cms::layout.base')

@section('content')

<div class="kameli-row">
    <h1>Menupunkter</h1>

    <div class="label label-success label-header-message">Rækkefølgen er gemt</div>

    <div class="header-btn-container">
        <a class="btn btn-primary btn-tab" href="{{ route('cms.menu.create') }}">Opret nyt menupunkt</a>
    </div>
</div>

<div class="kameli-row">

    <ol class="js-sortable vertical" data-sort-url="{{ route('cms.menu.update-order') }}">
        @foreach($menus as $menu)
        <li data-id="{{ $menu->id }}">
            <div class="sortable-row clearfix">
                <div class="sortable-title">
                    <div class="fa fa-item js-expand sortable-expand"></div>
                    <span>{{ $menu->name }}</span>
                </div>
                <div class="sortable-languages">
                    @foreach($menu->activeLanguages() as $lang)
                    <img src="{{ asset('packages/kameli/cms/img/flags/'.$lang['code'].'.png') }}" alt="{{ $lang['name_da'] }}" class="language-flag">
                    @endforeach
                </div>
                <div class="sortable-actions">
                    {{ link_to_route('cms.menu.edit', 'Rediger', $menu->id, ['class' => 'btn btn-xs btn-default']) }}
                    <div class="btn btn-xs btn-danger js-delete" data-url="{{ route('cms.menu.destroy', $menu->id) }}">Slet</div>
                    <i class="sort-handle js-handle fa fa-sort"></i>
                </div>
            </div>

            <ol data-id="{{ $menu->id }}">
                @if($menu->subMenus->count())
                @foreach($menu->subMenus as $submenu)
                <li data-id="{{ $submenu->id }}">
                    <div class="sortable-row clearfix">
                        <div class="sortable-title">
                            <div class="fa fa-item js-expand sortable-expand"></div>
                            <span>{{ $submenu->name }}</span>
                        </div>
                        <div class="sortable-languages">
                            @foreach($submenu->activeLanguages() as $lang)
                            <img src="{{ asset('packages/kameli/cms/img/flags/'.$lang['code'].'.png') }}" alt="{{ $lang['name_da'] }}" class="language-flag">
                            @endforeach
                        </div>
                        <div class="sortable-actions">
                            {{ link_to_route('cms.menu.edit', 'Rediger', $submenu->id, ['class' => 'btn btn-xs btn-default']) }}
                            <div class="btn btn-xs btn-danger js-delete" data-url="{{ route('cms.menu.destroy', $submenu->id) }}">Slet</div>
                            <i class="sort-handle js-handle fa fa-sort"></i>
                        </div>
                    </div>
                    <ol data-id="{{ $submenu->id }}">
                        @if($submenu->subMenus->count())
                        @foreach($submenu->subMenus as $submenu2)
                        <li data-id="{{ $submenu2->id }}">
                            <div class="sortable-row clearfix">
                                <div class="sortable-title">
                                    <div class="fa fa-item js-expand sortable-expand"></div>
                                    <span>{{ $submenu2->name }}</span>
                                </div>
                                <div class="sortable-languages">
                                    @foreach($submenu2->activeLanguages() as $lang)
                                    <img src="{{ asset('packages/kameli/cms/img/flags/'.$lang['code'].'.png') }}" alt="{{ $lang['name_da'] }}" class="language-flag">
                                    @endforeach
                                </div>
                                <div class="sortable-actions">
                                    {{ link_to_route('cms.menu.edit', 'Rediger', $submenu2->id, ['class' => 'btn btn-xs btn-default']) }}
                                    <div class="btn btn-xs btn-danger js-delete" data-url="{{ route('cms.menu.destroy', $submenu2->id) }}">Slet</div>
                                    <i class="sort-handle js-handle fa fa-sort"></i>
                                </div>
                            </div>
                        </li>
                        @endforeach
                        @endif
                    </ol>
                </li>
                @endforeach
                @endif
            </ol>

        </li>
        @endforeach
    </ol>
</div>

@stop

@section('js')
<script type="text/javascript">
    (function() {
        var $sortable = $('.js-sortable'),
            $message = $('.label-header-message');

        $sortable.sortable({
            onDrop: function($item, container, _super) {
                var parentId = container.el.data('id') || false,
                    ids = $.map(container.serialize(), function($o) { return $o.id; });

                $.post($sortable.data('sort-url'), {parentId: parentId, order: ids})
                    .success(function() {
                        $message.fadeIn().delay(2000).fadeOut();
                    });

                _super($item, container);
            },
            isValidTarget: function ($item, container) {
                var depth = 1, // Start with a depth of one (the element itself)
                    maxDepth = '{{ $max_menu_levels }}',
                    children = $item.find('ol').first().find('li');

                // Add the amount of parents to the depth
                depth += container.el.parents('ol').length;

                // Increment the depth for each time a child
                while (children.length) {
                    depth++;
                    children = children.find('ol').first().find('li');
                }

                return depth <= maxDepth;
            },
            handle: '.js-handle'
        });

        $('.js-expand').on('click', function() {
            $(this).closest('.sortable-row').next().slideToggle(200);
        });

        $('.js-delete').on('click', function() {
            var $btn = $(this);

            bootbox.setDefaults({locale: 'da'});
            bootbox.confirm('Er du sikker på at du vil slette menupunktet?', function(result) {
                if (result) window.location.href = $btn.data('url');
            });
        });
    })();
</script>
@stop