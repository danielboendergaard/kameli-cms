@extends('kameli/cms::layout.base')

@section('head')
<link rel="stylesheet" href="{{ asset('packages/kameli/cms/packages/select2-3.4.3/select2.css') }}">
<link rel="stylesheet" href="{{ asset('packages/kameli/cms/css/select2-bootstrap.css') }}">
@stop

@section('content')
{{ Form::open(['url' => route('cms.menu.update', $item->id), 'method' => 'patch']) }}

<div class="kameli-row">
    <h1>Rediger menupunkt</h1>

    <div class="header-btn-container">
        @foreach($languages as $id => $lang)
        <div class="btn-group">
            <a href="#{{ $lang->code }}" class="btn btn-primary btn-tab {{ $id == 0 ? 'active' : '' }}" data-toggle="tab">
                <span>{{ $lang->name_da }}</span>
            </a>
            <label class="btn btn-primary btn-tab">
                {{ Form::checkbox($lang->code.'[active]', 1, array_old_input($lang->code, 'active', $items)) }}
            </label>
        </div>
        @endforeach

        <button type="submit" class="btn btn-default btn-tab">Gem</button>
    </div>
</div>

<div class="kameli-row">

    <div class="tab-content">
        @foreach($languages as $id => $lang)

        <div id="{{ $lang->code }}" class="tab-pane fade {{ $id == 0 ? 'in active' : '' }}">

            <div class="kameli-section">
                <h3 class="kameli-section-header">Menupunktet</h3>

                <div class="form-group mbn">
                    {{ Form::label($lang->code.'[name]', 'Titel') }}
                    {{ Form::text($lang->code.'[name]', array_old_input($lang->code, 'name', $items), ['class' => 'form-control']) }}
                </div>

            </div>

            <div class="kameli-section">
                <h3 class="kameli-section-header">Menupunktets funktion</h3>

                <div class="js-link-function">
                    <div class="form-group">
                        {{ Form::label($lang->code.'[function_id]', 'Funktion') }}
                        {{ Form::select($lang->code.'[function_id]', $_functions, array_old_input($lang->code, 'function_id', $items), ['class' => 'form-control js-functions']) }}
                    </div>

                    <div class="form-group mbn js-option-1">
                        {{ Form::label($lang->code.'[page_id]', 'Side') }}
                        {{ Form::select($lang->code.'[page_id]', $_pages[$lang->id], array_old_input($lang->code, 'page_id', $items), ['class' => 'form-control select2']) }}
                    </div>

                    <div class="form-group mbn js-option-2">
                        {{ Form::label($lang->code.'[news_id]', 'Nyhed') }}
                        {{ Form::select($lang->code.'[news_id]', $_news, array_old_input($lang->code, 'news_id', $items), ['class' => 'form-control select2']) }}
                    </div>

                    <div class="form-group mbn js-option-3">
                        {{ Form::label($lang->code.'[url]', 'URL') }}
                        {{ Form::text($lang->code.'[url]', array_old_input($lang->code, 'url', $items), ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group mbn js-option-4">
                        {{ Form::label($lang->code.'[module]', 'Modul') }}
                        {{ Form::select($lang->code.'[module]', $_modules, array_old_input($lang->code, 'module', $items), ['class' => 'form-control']) }}
                    </div>
                </div>
            </div>

        </div>

        @endforeach
    </div>

    @if(Session::has('lang_errors'))
    <div class="alert alert-danger alert-block mtl mbn">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4>Indtastningsfejl</h4>

        @foreach(Session::get('lang_errors') as $lang => $errors)
        <strong>{{ $lang }}</strong><br>
        {{ implode('', $errors->all(':message<br>')) }}
        @endforeach
    </div>
    @endif

</div>

{{ Form::close() }}
@stop

@section('js')
<script type="text/javascript" src="{{ asset('packages/kameli/cms/packages/select2-3.4.3/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('packages/kameli/cms/packages/select2-3.4.3/select2_locale_da.js') }}"></script>
<script type="text/javascript">
    (function() {
        $('.select2').select2();

        $('.btn:first-child').on('click', function() {
            var $this = $(this);

            if ($this.hasClass('active')) return;

            $this.addClass('active').parent().siblings().find('.btn:first-child').removeClass('active');
        });

        /*
        |--------------------------------------------------------------------------
        | Hide unused inputs
        |--------------------------------------------------------------------------
        */

        $('.js-functions').on('change', function() {
            var $this = $(this),
                $parent = $this.closest('.js-link-function'),
                selector = '.js-option-' + $this.val();

            // Hide selects
            $parent.find('[class*="js-option-"]').hide();

            // Show the relevant input
            $parent.find(selector).show();
        }).change();
    })();
</script>
@stop