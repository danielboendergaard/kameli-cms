<!doctype html>
<html lang="da">
<head>
    <meta charset="UTF-8">
    <title>Mediemanager</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('packages/kameli/cms/css/backend.css') }}"/>
</head>
<body>

<div class="clearfix kameli-manager-files">

    @if(isset($backPath))
    <a href="{{ URL::to($backPath) }}" class="kameli-manager-item js-folder">
        <i class="folder-icon fa fa-folder"></i>
        <div class="kameli-manager-filename">..</div>
    </a>
    @endif

    @foreach($directories as $dir)
    <a href="{{ URL::current().'/'.$dir }}" class="kameli-manager-item js-folder" title="{{ $dir }}">
        <i class="folder-icon fa fa-folder"></i>
        <div class="kameli-manager-filename">{{ Str::limit($dir, 10) }}</div>
    </a>
    @endforeach

    @foreach($files as $file)
    <div class="kameli-manager-item" data-id="{{ $file->id }}" title="{{ $file->filename }}" data-url="{{ $file->getPublicPath() }}">
        <a href="{{ route('cms.upload.destroy', $file->id) }}" class="kameli-manager-delete">
            <i class="fa fa-times"></i>
        </a>
        <i class="folder-icon {{ $file->getIconClass() }}"></i>

        <div class="kameli-manager-filename">{{ Str::limit($file->filename, 10) }}</div>
    </div>
    @endforeach
</div>

<hr>

<div class="row mhn">
    <div class="col-xs-6 pln">
        {{ Form::open(['files' => true, 'class' => 'form-inline']) }}

        <div class="input-group">
            {{ Form::file('files[]', ['multiple', 'class' => 'form-control pls']) }}
            <span class="input-group-btn">
                {{ Form::submit('Upload', ['class' => 'btn btn-primary btn-addon']) }}
            </span>
        </div>

        {{ Form::close() }}
    </div>

    <div class="col-xs-6 prn">

        {{ Form::open(['class' => 'form-inline']) }}

        <div class="input-group">
            {{ Form::text('folder', null, ['class' => 'form-control', 'placeholder' => 'Ny mappe']) }}
            <span class="input-group-btn">
                {{ Form::submit('Opret', ['name' => 'new_folder', 'class' => 'btn btn-primary btn-addon']) }}
            </span>
        </div>

        {{ Form::close() }}
    </div>
</div>

</body>
</html>

