@extends('kameli/cms::layout.base')

@section('content')
{{ Form::open(['route' => 'cms.user.store']) }}

<div class="kameli-row">
    <h1>Opret ny bruger</h1>

    <div class="header-btn-container">
        <button type="submit" class="btn btn-primary btn-tab">Gem</button>
    </div>
</div>

<div class="kameli-row">
    <div class="kameli-section">
        <h3 class="kameli-section-header">Brugeroplysninger</h3>

        <div class="form-group">
            {{ Form::label('first_name', 'Fornavn') }}
            {{ Form::text('first_name', null, ['class' => 'form-control']) }}
        </div>

        <div class="form-group">
            {{ Form::label('last_name', 'Efternavn') }}
            {{ Form::text('last_name', null, ['class' => 'form-control']) }}
        </div>

        <div class="form-group">
            {{ Form::label('email', 'E-mail') }}
            {{ Form::text('email', null, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="kameli-section">
        <h3 class="kameli-section-header">Brugerrettigheder</h3>

        @foreach($permissions as $index => $permission)
        <div class="checkbox">
            <label>
                {{ Form::checkbox("permissions[$index]", 1, Input::old('permissions.'.$index)) }}
                <span>{{ $permission }}</span>
            </label>
        </div>
        @endforeach
    </div>

    @include('kameli/cms::_partials.form-messages')
</div>

{{ Form::close() }}
@stop