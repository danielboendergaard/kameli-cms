@extends('kameli/cms::layout.base')

@section('content')
<div class="kameli-row">
    <h1>Brugere</h1>

    <div class="header-btn-container">
        <a class="btn btn-primary btn-tab" href="{{ route('cms.user.create') }}">Opret ny bruger</a>
    </div>
</div>

<div class="kameli-row">

    <table class="table table-striped kameli-table">
        <thead>
        <tr>
            <th>E-mail</th>
            <th>Navn</th>
            <th>Sidste login</th>
            <th>Oprettet</th>
            <th>Handlinger</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{ $user->email }}</td>
            <td>{{ $user->first_name }} {{ $user->last_name }}</td>
            <td>{{ $user->last_login }}</td>
            <td>{{ $user->created_at }}</td>
            <td>
                {{ link_to_route('cms.user.edit', 'Rediger', $user->id, ['class' => 'btn btn-xs btn-default']) }}
                <div class="btn btn-xs btn-danger js-delete" data-url="{{ route('cms.user.destroy', $user->id) }}">Slet</div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

</div>
@stop

@section('js')
<script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    (function() {
        $('.js-delete').on('click', function() {
            var $btn = $(this);

            bootbox.setDefaults({locale: 'da'});
            bootbox.confirm('Er du sikker på at du vil slette brugeren?', function(result) {
                if (result) window.location.href = $btn.data('url');
            });
        });
    })();
</script>
@stop