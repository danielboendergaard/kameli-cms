@extends('kameli/cms::layout.base')

@section('content')

<h1 class="kameli-row">Ret bruger</h1>

<div class="kameli-row">

    <div class="row">
        <div class="col-md-6">
            <div class="kameli-section">
                <h3 class="kameli-section-header">Ret dine brugeroplysninger</h3>

                {{ Form::model($user, ['route' => 'cms.user.store-user']) }}

                <div class="form-group">
                    {{ Form::label('email', 'E-mail') }}
                    {{ Form::text('email', null, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('first_name', 'Navn') }}
                    {{ Form::text('first_name', null, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('last_name', 'Efternavn') }}
                    {{ Form::text('last_name', null, ['class' => 'form-control']) }}
                </div>

                @include('kameli/cms::_partials.form-messages')

                {{ Form::submit('Gem', ['class' => 'btn btn-primary']) }}

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@stop