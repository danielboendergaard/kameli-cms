@extends('kameli/cms::layout.auth')

@section('content')
<div class="kameli-login">
    <div class="kameli-section">
        <h4 class="kameli-section-header">Aktiver bruger</h4>
        <div class="kameli-section-content">
            {{ Form::open() }}

            {{ Form::text('email', null, ['class' => 'form-control mbm', 'placeholder' => 'E-mail adresse', 'autofocus']) }}
            {{ Form::password('password', ['class' => 'form-control mbm', 'placeholder' => 'Ønsket kode']) }}
            {{ Form::password('password_confirmation', ['class' => 'form-control mbm', 'placeholder' => 'Gentag ønsket kode']) }}

            @include('kameli/cms::_partials.form-messages')

            {{ Form::submit('Aktiver', ['class' => 'btn btn-default']) }}

            {{ Form::close() }}
        </div>
    </div>
</div>
@stop