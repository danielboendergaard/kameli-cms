@extends('kameli/cms::layout.base')

@section('content')

<h1 class="kameli-row">Skift kode</h1>

<div class="kameli-row">

    <div class="row">
        <div class="col-md-6">
            <div class="kameli-section">
                <h3 class="kameli-section-header">Skift din kode</h3>

                {{ Form::open(['route' => 'cms.user.store-password']) }}

                <div class="form-group">
                    {{ Form::label('current_password', 'Nuværende kode') }}
                    {{ Form::password('current_password', ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('new_password', 'Ny kode') }}
                    {{ Form::password('new_password', ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('new_password_confirmation', 'Gentag ny kode') }}
                    {{ Form::password('new_password_confirmation', ['class' => 'form-control']) }}
                </div>

                @include('kameli/cms::_partials.form-messages')

                {{ Form::submit('Gem', ['class' => 'btn btn-primary']) }}

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@stop