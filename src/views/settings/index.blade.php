@extends('kameli/cms::layout.base')

@section('content')
{{ Form::model($settings) }}

<div class="kameli-row">
    <h1>Indstillinger</h1>

    <div class="header-btn-container">
        <button type="submit" class="btn btn-primary btn-tab">Gem</button>
    </div>
</div>

<div class="kameli-row">
    <div class="kameli-section">
        <h4 class="kameli-section-header">Globale indstillinger</h4>

        <div class="form-group">
            {{ Form::label('website_name', 'Hjemmsidens navn') }}
            {{ Form::text('website_name', null, ['class' => 'form-control']) }}
        </div>

        <div class="form-group">
            {{ Form::label('online', 'Online / Offline') }}
            {{ Form::select('online', [1 => 'Online', 0 => 'Offline'], null, ['class' => 'form-control']) }}
        </div>

        <div class="form-group">
            {{ Form::label('offline_message', 'Offline besked') }}
            {{ Form::textarea('offline_message', null, ['class' => 'form-control']) }}
        </div>

        <div class="form-group">
            {{ Form::label('index', 'Indekser') }}
            {{ Form::select('index', [1 => 'Ja', 0 => 'Nej'], null, ['class' => 'form-control']) }}
        </div>


        <div class="row">
            <div class="form-group col-xs-4">
                {{ Form::label('tracking_type', 'Tracking') }}
                {{ Form::select('tracking_type', $trackingTypes, null, ['class' => 'form-control']) }}
            </div>

            <div class="form-group col-xs-8">
                {{ Form::label('tracking_id', 'Tracking ID') }}
                {{ Form::text('tracking_id', null, ['class' => 'form-control']) }}
            </div>
        </div>

        @include('kameli/cms::_partials.form-messages')
    </div>
</div>

{{ Form::close() }}
@stop