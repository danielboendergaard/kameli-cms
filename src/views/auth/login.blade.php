@extends('kameli/cms::layout.auth')

@section('content')
<div class="kameli-login">
    <div class="kameli-section">
        <h4 class="kameli-section-header">Log ind</h4>
        <div class="kameli-section-content">
            {{ Form::open() }}

            {{ Form::text('email', null, ['class' => 'form-control mbm', 'placeholder' => 'E-mail adresse', 'autofocus']) }}
            {{ Form::password('password', ['class' => 'form-control mbm', 'placeholder' => 'Kode']) }}

            {{ Form::submit('Log ind', ['class' => 'btn btn-kameli']) }}

            {{ Form::close() }}

            @include('kameli/cms::_partials.form-messages')
        </div>
    </div>
</div>
@stop