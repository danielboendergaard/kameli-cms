@section('head')
@parent
<link rel="stylesheet" href="{{ asset('packages/kameli/cms/packages/select2-3.4.3/select2.css') }}">
<link rel="stylesheet" href="{{ asset('packages/kameli/cms/css/select2-bootstrap.css') }}">
@stop

<div class="js-link-function">
    <div class="form-group">
        {{ Form::label('function_id', 'Funktion') }}
        {{ Form::select('function_id', $_functions, null, ['class' => 'form-control js-functions']) }}
    </div>

    <div class="form-group mbn js-option-1">
        {{ Form::label('page_id', 'Side') }}
        {{ Form::select('page_id', $_pages, null, ['class' => 'form-control select2']) }}
    </div>

    <div class="form-group mbn js-option-2">
        {{ Form::label('news_id', 'Nyhed') }}
        {{ Form::select('news_id', $_news, null, ['class' => 'form-control select2']) }}
    </div>

    <div class="form-group mbn js-option-3">
        {{ Form::label('url', 'URL') }}
        {{ Form::text('url', null, ['class' => 'form-control js-url', 'placeholder' => 'http://']) }}
    </div>

    <div class="form-group mbn js-option-4">
        {{ Form::label('module', 'Modul') }}
        {{ Form::select('module', $_modules, null, ['class' => 'form-control']) }}
    </div>
</div>

@section('js')
@parent
<script type="text/javascript" src="{{ asset('packages/kameli/cms/packages/select2-3.4.3/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('packages/kameli/cms/packages/select2-3.4.3/select2_locale_da.js') }}"></script>
<script type="text/javascript">
    (function() {
        $('.select2').select2();

        $('.js-functions').on('change', function() {
            var $this = $(this),
                $parent = $this.closest('.js-link-function'),
                selector = '.js-option-' + $this.val();

            // Hide selects
            $parent.find('[class*="js-option-"]').hide();

            // Show the relevant input
            $parent.find(selector).show();
        }).change();

        $('.js-url').on('focus', function() {
            var $this = $(this);

            if ($this.val() == '') {
                $this.val($this.attr('placeholder'));
            }
        })
    })();
</script>
@stop