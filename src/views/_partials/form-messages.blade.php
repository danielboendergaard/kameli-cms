@if($errors->any())

<div class="alert alert-danger alert-block mtm">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4>Indtastningsfejl</h4>
    {{ implode('', $errors->all(':message<br>')) }}
</div>

@elseif(Session::has('success'))

<div class="alert alert-success alert-block mtm">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    {{ Session::get('success') }}
</div>

@endif