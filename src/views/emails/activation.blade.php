<!doctype html>
<html lang="da-DK">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>

<p>
    Hej {{ $name }}
</p>

<p>
    Du er blevet oprettet som bruger på <a href="{{ $base_url }}">{{ $base_url }}</a>, for at aktivere din bruger skal du gå til<br>
    <a href="{{ $activation_url }}">{{ $activation_url }}</a>
</p>

</body>
</html>