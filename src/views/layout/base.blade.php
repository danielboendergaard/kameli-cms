<!doctype html>
<html lang="da-DK">
<head>
    <meta charset="UTF-8">
    <title>@yield('title', $_name)</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ $_mainCss }}"/>

    @foreach (isset($_stylesheets) ? $_stylesheets : [] as $stylesheet)
    <link rel="stylesheet" href="{{ asset($stylesheet) }}">
    @endforeach

    @yield('head')
</head>
<body>

@section('cms-header')
<div class="navbar navbar-fixed-top kameli-navbar">
    <img src="{{ asset('packages/kameli/cms/img/k-logo.png') }}" alt="Kameli" class="kameli-logo">
    <a href="/" class="kameli-title">{{ $_name }}</a>

    <div class="pull-right">
        <span>Velkommen, {{ $_user->first_name }} {{ $_user->last_name }}</span>

        <div class="dropdown kameli-options">
            <a href="#" data-toggle="dropdown"> <i class="fa fa-cogs"></i></a>
            <ul class="dropdown-menu">
                <li><a href="{{ route('cms.user.edit-user') }}" class="kameli-options">Ret bruger</a></li>
                <li><a href="{{ route('cms.user.edit-password') }}" class="kameli-options">Skift kode</a></li>
            </ul>
        </div>

        <div class="kameli-seperator"></div>

        <a href="{{ route('cms.logout') }}" class="btn btn-kameli">Log af</a>
    </div>
</div>
@show

<div class="kameli-wrapper clearfix">
    @section('cms-menu')
    <div class="kameli-menu">
        @foreach($_menu as $section)
        <div class="kameli-menu-header">{{ $section->getTitle() }} <div class="kameli-menu-arrow"></div></div>
        <div class="kameli-menu-items {{ Request::cookie('menu_'.md5($section->getTitle())) ? 'collapsed' : '' }}">
            @foreach($section as $item)
            <a href="{{ $item->getUrl() }}" class="kameli-menu-item {{ $item->getActiveClass() }}" {{ $item->getAttributes() }}>
                <i class="{{ $item->getIcon() }} fa-fw"></i>
                <span>{{ $item->getTitle() }}</span>
            </a>
            @endforeach
        </div>
        @endforeach

        <a href="http://kameli.dk" target="_blank">
            <img src="{{ asset('packages/kameli/cms/img/kameli-logo.png') }}" alt="Kameli" class="mtl">
        </a>
    </div>
    @show

    <div class="kameli-content">
        @yield('content')
    </div>
</div>

<div class="modal fade" id="media-modal" data-url="{{ route('cms.media.index') }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Mediemanager</h4>
            </div>
            <div class="modal-body">
                <iframe src="" class="js-media"></iframe>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="upload-modal" data-url="{{ route('cms.upload.index') }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Filmanager</h4>
            </div>
            <div class="modal-body">
                <iframe src="" class="js-uploads"></iframe>
            </div>
        </div>
    </div>
</div>

<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<script src="{{ asset('packages/kameli/cms/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('packages/kameli/cms/ckeditor/adapters/jquery.js') }}"></script>
<script src="{{ asset('packages/kameli/cms/js/jquery.are-you-sure.js') }}"></script>
<script src="{{ asset('packages/kameli/cms/js/jquery-sortable.js') }}"></script>
<script src="{{ asset('packages/kameli/cms/js/jquery.mediamanager.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.3.0/bootbox.min.js"></script>
<script>
    (function() {
        $('.kameli-menu-header').on('click', function() {
            var $this = $(this),
                $items = $this.next(),
                url = '{{ route("cms.menu.store-section-status") }}';

            $items.slideToggle(function() {
                $items.toggleClass('collapsed');

                $.post(url, {
                    page: $this.text().trim(),
                    value: $items.hasClass('collapsed') ? 1 : 0
                }, function(data) {

                });
            });
        });
    })();
</script>

@foreach (isset($_scripts) ? $_scripts : [] as $script)
<script src="{{ asset($script) }}"></script>
@endforeach

@yield('js')
</body>
</html>