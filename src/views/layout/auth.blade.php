<!doctype html>
<html lang="da-DK">
<head>
    <meta charset="UTF-8">
    <title>{{ $_name }}</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('packages/kameli/cms/css/backend.css') }}"/>
</head>
<body>

<div class="navbar navbar-fixed-top kameli-navbar">
    <img src="{{ asset('packages/kameli/cms/img/k-logo.png') }}" alt="Kameli" class="kameli-logo">
    <a href="/" class="kameli-title">{{ $_name }}</a>
</div>

<div class="kameli-content">
    @yield('content')
</div>

</body>
</html>