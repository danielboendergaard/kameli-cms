@extends('kameli/cms::layout.base')

@section('content')
{{ Form::open(['url' => route('cms.page.update', $page->id), 'method' => 'patch']) }}

<div class="kameli-row">
    <h1>Rediger side</h1>

    <div class="label label-default label-header-message">Du har ugemte ændringer</div>

    <div class="header-btn-container">
        @foreach($languages as $id => $lang)
        <div class="btn-group">
            <a href="#{{ $lang->code }}" class="btn btn-primary btn-tab {{ $id == 0 ? 'active' : '' }}" data-toggle="tab">
                <span>{{ $lang->name_da }}</span>
            </a>
            <label class="btn btn-primary btn-tab">
                {{ Form::checkbox('', 1, null, ['class' => 'js-published-'.$lang->code, 'data-ays-ignore' => 'true']) }}
            </label>
        </div>
        @endforeach

        {{ Form::submit('Gem', ['name' => '_save-and-stay', 'class' => 'btn btn-default btn-tab']) }}
        {{ Form::submit('Gem og luk', ['class' => 'btn btn-default btn-tab']) }}
    </div>
</div>

<input type="hidden" name="using_template" value="{{ Input::old('template', reset($pages)->template) }}">

@if(Session::has('lang_errors'))
<div class="alert alert-danger alert-block mtl mhxl mbn">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4>Indtastningsfejl</h4>

    @foreach(Session::get('lang_errors') as $lang => $errors)
    <strong>{{ $lang }}</strong><br>
    {{ implode('', $errors->all(':message<br>')) }}
    @endforeach
</div>
@endif

<div class="tab-content">
    @foreach($languages as $id => $lang)
    <div id="{{ $lang->code }}" class="tab-pane fade {{ $id == 0 ? 'in active' : '' }}">
        <div class="kameli-row">
            <div class="row">
                <div class="col-lg-8">
                    <div class="kameli-section mbn">
                        <h4 class="kameli-section-header">Egenskaber</h4>

                        <div class="row">
                            <div class="form-group col-lg-12">
                                {{ Form::label($lang->code.'[title]', 'Titel') }}
                                {{ Form::text($lang->code.'[title]', array_old_input($lang->code, 'title', $pages), ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-6">
                                {{ Form::label($lang->code.'[published]', 'Offentlig') }}
                                {{ Form::select($lang->code.'[published]', [0 => 'Nej', 1 => 'Ja'], array_old_input($lang->code, 'published', $pages), ['class' => 'form-control js-published-select']) }}
                            </div>

                            <div class="form-group col-lg-6">
                                {{ Form::label($lang->code.'[category_id]', 'Kategori') }}
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a href="#category-modal" class="btn btn-primary btn-addon js-add-category" data-toggle="modal">Tilføj</a>
                                    </span>
                                    {{ Form::select($lang->code.'[category_id]', $categories, array_old_input($lang->code, 'category_id', $pages), ['class' => 'form-control js-categories']) }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label($lang->code.'[meta_description]', 'META description') }}
                            {{ Form::textarea($lang->code.'[meta_description]', array_old_input($lang->code, 'meta_description', $pages), ['class' => 'form-control', 'rows' => 2]) }}
                        </div>
                    </div>
                </div>

                @if($id == 0)
                <div class="col-lg-4">
                    <div class="kameli-section">
                        <h4 class="kameli-section-header">Skabelon</h4>

                        <div class="form-group">
                            {{ Form::label('template', 'Vælg skabelon') }}
                            {{ Form::select('template', [0 => 'Ingen skabelon', 1 => 'To kolonner'], array_old_input($lang->code, 'template', $pages), ['class' => 'form-control js-submit ays-ignore']) }}
                        </div>
                    </div>
                </div>
                @endif

            </div>
        </div>

        @if(Input::old('template', $pages[$defaultLanguage]->template))

        <div class="kameli-row">

            <div class="row">
                <div class="col-md-6">
                <p><strong>Venstre kolonne</strong></p>

                    @if($id == 0)

                    <div class="form-group">
                        <div class="btn-group btn-group-justified js-tab-buttons">
                            <label class="btn btn-default" data-target="js-text-left">
                                {{ Form::radio('left_content_type', 'text', $pages[$lang->code]->main_content_type == 'text') }} Tekst
                            </label>
                            <label class="btn btn-default" data-target="js-images-left">
                                {{ Form::radio('left_content_type', 'images', $pages[$lang->code]->main_content_type == 'images') }} Billeder
                            </label>
                            <label class="btn btn-default" data-target="js-video-left">
                                {{ Form::radio('left_content_type', 'video', $pages[$lang->code]->main_content_type == 'video') }} Video
                            </label>
                        </div>
                    </div>

                    @endif

                    <div class="tab-content">
                        <div class="tab-pane js-text-left">
                            {{ Form::textarea($lang->code.'[main_content]', array_old_input($lang->code, 'main_content', $pages), ['class' => 'form-control js-ckeditor', 'id' => 'cke-'.$lang->code]) }}

                            <button class="btn btn-primary mtm" data-toggle="mediamanager" data-content="text" data-instance="cke-{{ $lang->code }}">Indsæt billede</button>
                            <button class="btn btn-primary mtm" data-toggle="mediamanager" data-content="text" data-instance="cke-{{ $lang->code }}" data-target="#upload-modal" data-type="files">Indsæt fil</button>
                        </div>

                        <div class="tab-pane js-images-left">
                            <div id="{{ $lang->code }}-left-images" class="clearfix mbl js-images-container-left">
                                @if(isset($pages[$lang->code]))
                                @foreach($pages[$lang->code]->mainImages() as $image)
                                <div class="kameli-post-image">
                                    <img src="{{ asset($image->thumbnail()) }}" alt="" class="img-responsive absolute-center"/>
                                    <input type="hidden" name="{{$lang->code}}[images_left][]" value="{{ $image->id }}">
                                </div>
                                @endforeach
                                @endif
                            </div>

                            <button type="button" class="btn btn-primary" data-toggle="mediamanager" data-content="images" data-target="#{{ $lang->code }}-left-images" data-name="{{$lang->code}}[images_left][]">Tilføj billede</button>

                            @if($id == 0)
                            <div class="btn btn-primary pull-right js-copy-images" data-target=".js-images-container-left">Kopier til andre sprog</div>
                            @endif
                        </div>

                        <div class="tab-pane js-video-left clearfix">
                            <div class="form-group">
                                {{ Form::label($lang->code.'[main_video]', 'Youtube URL') }}
                                <div class="input-group">
                                    {{ Form::text($lang->code.'[main_video]', array_old_input($lang->code, 'main_video', $pages), ['class' => 'form-control js-video-input-left']) }}
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-addon js-check-yt">Tjek</button>
                                    </span>
                                </div>
                            </div>

                            <div class="youtube-container"></div>

                            @if($id == 0)
                            <div class="btn btn-primary pull-right js-copy-video" data-target=".js-video-input-left">Kopier til andre sprog</div>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <p><strong>Højre kolonne</strong></p>

                    @if($id == 0)

                    <div class="form-group">
                        <div class="btn-group btn-group-justified js-tab-buttons">
                            <label class="btn btn-default" data-target="js-text-right">
                                {{ Form::radio('right_content_type', 'text', $pages[$lang->code]->right_content_type == 'text') }} Tekst
                            </label>
                            <label class="btn btn-default" data-target="js-images-right">
                                {{ Form::radio('right_content_type', 'images', $pages[$lang->code]->right_content_type == 'images') }} Billeder
                            </label>
                            <label class="btn btn-default" data-target="js-video-right">
                                {{ Form::radio('right_content_type', 'video', $pages[$lang->code]->right_content_type == 'video') }} Video
                            </label>
                        </div>
                    </div>

                    @endif

                    <div class="tab-content">
                        <div class="tab-pane js-text-right">
                            {{ Form::textarea($lang->code.'[right_content]', array_old_input($lang->code, 'right_content', $pages), ['class' => 'form-control js-ckeditor', 'id' => 'cke-'.$lang->code.'-right']) }}

                            <button class="btn btn-primary mtm" data-toggle="mediamanager" data-content="text" data-instance="cke-{{ $lang->code }}-right">Indsæt billede</button>
                            <button class="btn btn-primary mtm" data-toggle="mediamanager" data-content="text" data-instance="cke-{{ $lang->code }}-right" data-target="#upload-modal" data-type="files">Indsæt fil</button>
                        </div>

                        <div class="tab-pane js-images-right">
                            <div id="{{ $lang->code }}-right-images" class="clearfix mbl js-images-container-right">
                                @if(isset($pages[$lang->code]))
                                @foreach($pages[$lang->code]->rightImages() as $image)
                                <div class="kameli-post-image">
                                    <img src="{{ asset($image->thumbnail()) }}" alt="" class="img-responsive absolute-center"/>
                                    <input type="hidden" name="{{$lang->code}}[images_right][]" value="{{ $image->id }}">
                                </div>
                                @endforeach
                                @endif
                            </div>

                            <button type="button" class="btn btn-primary" data-toggle="mediamanager" data-content="images" data-target="#{{ $lang->code }}-right-images" data-name="{{$lang->code}}[images_right][]">Tilføj billede</button>

                            @if($id == 0)
                            <div class="btn btn-primary pull-right js-copy-images" data-target=".js-images-container-right">Kopier til andre sprog</div>
                            @endif
                        </div>

                        <div class="tab-pane js-video-right clearfix">
                            <div class="form-group">
                                {{ Form::label($lang->code.'[right_video]', 'Youtube URL') }}
                                <div class="input-group">
                                    {{ Form::text($lang->code.'[right_video]', array_old_input($lang->code, 'right_video', $pages), ['class' => 'form-control js-video-input-right']) }}
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-addon js-check-yt">Tjek</button>
                                    </span>
                                </div>
                            </div>

                            <div class="youtube-container"></div>

                            @if($id == 0)
                            <div class="btn btn-primary pull-right js-copy-video" data-target=".js-video-input-right">Kopier til andre sprog</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @else

        <div class="kameli-row">
            <div class="form-group mbn">
                {{ Form::label($lang->code.'[main_content]', 'Indhold') }}
                {{ Form::textarea($lang->code.'[main_content]', array_old_input($lang->code, 'main_content', $pages), ['class' => 'form-control js-ckeditor', 'id' => 'cke-'.$lang->code]) }}

                <button class="btn btn-primary mtm" data-toggle="mediamanager" data-content="text" data-instance="cke-{{ $lang->code }}">Indsæt billede</button>
                <button class="btn btn-primary mtm" data-toggle="mediamanager" data-content="text" data-instance="cke-{{ $lang->code }}" data-target="#upload-modal" data-type="files">Indsæt fil</button>
            </div>
        </div>

        @endif
    </div>
    @endforeach
</div>

{{ Form::close() }}

<div class="modal fade" id="category-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            {{ Form::open(['route' => 'cms.category.store', 'class' => 'js-add-category-form']) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tilføj kategori</h4>
            </div>
            <div class="modal-body">
                {{ Form::label('category', 'Kategori') }}
                {{ Form::text('category', null, ['class' => 'form-control js-category-name ays-ignore']) }}
                <div class="modal-error"></div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Tilføj</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript">
    (function() {

        /*
         |--------------------------------------------------------------------------
         | Set the active button in the option groups
         |--------------------------------------------------------------------------
         */

        var $buttons = $('.js-tab-buttons');

        $buttons.find('input').hide();

        $buttons.find('label').on('click', function() {
            var $this = $(this),
                $target = $('.'+$this.data('target'));

            $this.siblings().removeClass('active');
            $this.addClass('active');

            $target.siblings().removeClass('active');
            $target.addClass('active');
        });

        $buttons.each(function(key, item) {
            var $btn = $(item).find('input:checked').parent();

            $btn.addClass('active');

            $('.' + $btn.data('target')).addClass('active');
        });

        $('.js-check-yt').on('click', function() {
            var ex = /^.*(youtu.be\/|v\/|e\/|u\/\w+\/|embed\/|v=)([^#\&\?]*).*/,
                matches = $(this).parent().prev().val().match(ex);

            if ( ! matches) {
                alert('Ugyldig youtube url');
                return;
            }

            var videoId = matches[2];

            $(this).closest('.tab-pane').find('.youtube-container').html(
                '<iframe src="//www.youtube.com/embed/' + videoId + '?rel=0" frameborder="0" width="560" height="315"></iframe>'
            );
        });

        /*
         |--------------------------------------------------------------------------
         | Submit on template change
         |--------------------------------------------------------------------------
         */

        $('.js-submit').on('change', function() {
            $(window).unbind('beforeunload');

            $(this).closest('form').submit();
        });

        /*
         |--------------------------------------------------------------------------
         | Control tab button states
         |--------------------------------------------------------------------------
         */

        $('.btn:first-child').on('click', function() {
            var $this = $(this);

            if ($this.hasClass('active')) return;

            $this.tab('show');

            $this.addClass('active').parent().siblings().find('.btn:first-child').removeClass('active');
        });

        /*
         |--------------------------------------------------------------------------
         | Detect changes in forms
         |--------------------------------------------------------------------------
         */

        var $messageLabel = $('.label-header-message');

        $('form').areYouSure({message: 'Du har ændringer der ikke er gemt', change: function(form) {
            form.hasClass('dirty') ? $messageLabel.fadeIn() : $messageLabel.fadeOut();
        }});

        /*
         |--------------------------------------------------------------------------
         | CKEditor
         |--------------------------------------------------------------------------
         */

        // Instantiate CKEditor
        $('.js-ckeditor').ckeditor();

        // Fire change event on textarea when ckeditor instances register changes
        $.each(CKEDITOR.instances, function(index, item) {
            var $element = $(item.element.$);

            item.on('change', function() {
                $element.change();
            })
        });

        /*
         |--------------------------------------------------------------------------
         | Add categories
         |--------------------------------------------------------------------------
         */

        var $modal = $('#category-modal'),
            $categories = $('.js-categories'),
            $nameField = $('.js-category-name'),
            $modalErrors = $('.modal-error');

        // Put input field on focus when modal is loaded
        $modal.on('shown.bs.modal', function() {
            $nameField.trigger('focus');
        });

        $('.js-add-category-form').on('submit', function(e) {
            e.preventDefault();

            $.post(this.action, {name: $nameField.val()}, function(data) {
                if (data.status == 'error') {
                    // Show errors in modal
                    $modalErrors.html($('<div>', {'class': 'alert alert-block alert-danger mtm mbn', text: data.message}));
                } else {
                    // Insert the new option and select it
                    $categories.append($('<option>', {value: data.id, text: data.name})).val(data.id);

                    // Clear modal
                    $nameField.val('');
                    $modalErrors.html('');

                    // Hide modal
                    $modal.modal('hide');
                }
            });
        });

        /*
         |--------------------------------------------------------------------------
         | Keep published status in sync
         |--------------------------------------------------------------------------
         */

        $('[class^="js-published"]').on('change', function() {
            var $this = $(this),
                selector = "[name='"+ $this.attr('class').substr(-2)  +"[published]']";

            $(selector).val($this.is(':checked') ? 1 : 0).change();
        });

        $('.js-published-select').on('change', function() {
            var code = $(this).attr('name').substr(0, 2);

            $('.js-published-'+code).attr('checked', 1 == $(this).val());
        }).change();

        /*
         |--------------------------------------------------------------------------
         | Remove post images on click
         |--------------------------------------------------------------------------
         */

        $('.tab-pane').on('click', '.kameli-post-image', function() {
            $(this).remove();
        });


        $('.js-copy-images').on('click', function() {
            if ( ! confirm('Dette vil fjerne billederne i de andre sprog og erstatte dem med billederne herfra')) {
                return;
            }

            var langs = '{{ $languages->implode("code", ",")}}'.split(',');

            var $this = $(this),
                $targets = $($this.data('target')),
                content = $targets.first().html();

            $targets.slice(1).each(function(index, item) {
                $(item).html(content.split(langs[0] + '[').join(langs[index + 1] + '['));
            });
        });

        $('.js-copy-video').on('click', function() {
            if ( ! confirm('Dette vil fjerne linket i de andre sprog og erstatte det med linket herfra')) {
                return;
            }

            var $this = $(this),
                $targets = $($this.data('target')),
                value = $targets.first().val();

            $targets.val(value);
        });
    })();
</script>
@stop