@extends('kameli/cms::layout.base')

@section('content')
<div class="kameli-row">
    <h1>Sider</h1>

    <div class="header-btn-container">
        <a href="{{ route('cms.page.create') }}" class="btn btn-primary btn-tab">Opret ny side</a>
    </div>
</div>

<div class="kameli-row">

    <div class="input-group form-group">
        <div class="input-group-btn">
            <button class="btn btn-kameli btn-addon" type="button" data-toggle="dropdown"><span class="js-category">Filtrer</span> <span class="caret"></button>
            <ul class="dropdown-menu js-categories"></ul>
        </div>
        <input type="text" class="form-control js-filter" placeholder="Søg her">
    </div>

    <table class="table table-striped kameli-table">
        <thead>
        <tr>
            <th>Titel</th>
            <th>Offentlige sprog</th>
            <th>Kategori</th>
            <th>Oprettet</th>
            <th>Handlinger</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pages as $page)
        <tr>
            <td>{{ $page->title }}</td>
            <td>{{ $page->publishedLanguages()->implode('name_da', ', ') }}</td>
            <td>{{ $page->category->name }}</td>
            <td>{{ $page->created_at }}</td>
            <td>
                {{ link_to_route('cms.page.edit', 'Rediger', $page->id, ['class' => 'btn btn-xs btn-default']) }}
                <div class="btn btn-xs btn-danger js-delete" data-url="{{ route('cms.page.destroy', $page->id) }}">Slet</div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    @include('kameli/cms::_partials.form-messages')

    @if (Session::has('related'))
    <div class="alert alert-danger alert-block mtm">
        <p>Siden kan ikke slettes da den bliver linket til fra følgende:</p>

        @foreach (Session::get('related') as $type => $items)
        <p>
            <strong>{{ $type }}</strong><br>

            @foreach ($items as $item)
            <span>{{ $item->linkable->getDisplayName() }}</span><br>
            @endforeach
        </p>
        @endforeach
    </div>
    @endif

</div>
@stop

@section('js')
<script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    (function() {
        $.fn.dataTableExt.oApi.fnGetColumnData = function (oSettings, iColumn, bUnique, bFiltered, bIgnoreEmpty) {

            // check that we have a column id
            if ( typeof iColumn == "undefined" ) return [];

            // by default we only wany unique data
            if ( typeof bUnique == "undefined" ) bUnique = true;

            // by default we do want to only look at filtered data
            if ( typeof bFiltered == "undefined" ) bFiltered = true;

            // by default we do not wany to include empty values
            if ( typeof bIgnoreEmpty == "undefined" ) bIgnoreEmpty = true;

            // list of rows which we're going to loop through
            var aiRows;

            // use only filtered rows
            if (bFiltered == true) aiRows = oSettings.aiDisplay;
            // use all rows
            else aiRows = oSettings.aiDisplayMaster; // all row numbers

            // set up data array
            var asResultData = [];

            for (var i=0,c=aiRows.length; i<c; i++) {
                iRow = aiRows[i];
                var aData = this.fnGetData(iRow);
                var sValue = aData[iColumn];

                // ignore empty values?
                if (bIgnoreEmpty == true && sValue.length == 0) continue;

                // ignore unique values?
                else if (bUnique == true && jQuery.inArray(sValue, asResultData) > -1) continue;

                // else push the value onto the result data array
                else asResultData.push(sValue);
            }

            return asResultData;
        };

        // Set custom css classes
        $.fn.dataTableExt.oStdClasses.sPagePrevEnabled = 'btn btn-kameli';
        $.fn.dataTableExt.oStdClasses.sPageNextEnabled = 'btn btn-kameli';
        $.fn.dataTableExt.oStdClasses.sPageNextDisabled = 'btn btn-kameli disabled';
        $.fn.dataTableExt.oStdClasses.sPagePrevDisabled = 'btn btn-kameli disabled';
        $.fn.dataTableExt.oStdClasses.sPaging = 'btn-group dataTables_paginate paging_';

        var $table = $('.table').dataTable({
            sDom: "<'row'r>t<'row'<'col-md-4'l><'col-md-4'i><'col-md-4'p>>",
            oLanguage: {
                "sLengthMenu": "Vis _MENU_ linier per side",
                "sZeroRecords":  "Ingen sider matcher søgningen",
                "sSearch": "Søg",
                "sInfo": "Viser _START_ til _END_ af _TOTAL_",
                "sInfoEmpty":    "Viser 0 linier",
                "sInfoFiltered": "(filtreret fra _MAX_ linier)",
                "oPaginate": {
                    "sPrevious": "Forrige",
                    "sNext": "Næste"
                }
            },
            aLengthMenu: [[10, 20, -1], [10, 20, "Alle"]],
            aoColumnDefs: [
                {
                    'bSortable': false,
                    'aTargets': [ -1 ]
                }
            ]
        });

        // Add categories to dropdown
        var $category = $('.js-category'),
            categoryOriginalText = $category.text(),
            $categories = $('.js-categories'),
            appendage = '<li><a href="#" data-filter="" class="js-clear">Ryd filtrering</a></li>';

        $.each($table.fnGetColumnData(2), function(i, item) {
            appendage += '<li><a href="#" data-filter="' + item + '">' + item + '</a></li>';
        });

        $categories.html(appendage);

        var $clear = $categories.find('.js-clear').hide();

        $categories.on('click', 'a', function(e) {
            var $this = $(this);
            e.preventDefault();

            $table.fnFilter($this.data('filter'), 2);

            if ($this.data('filter')) {
                $category.text($this.data('filter'));
            } else {
                $category.text(categoryOriginalText);
            }

            $clear.toggle( ! $this.is($clear));
        });

        // Attach filter event to input
        $('.js-filter').on('keyup', function() {
            $table.fnFilter($(this).val(), 0);
        });

        $('.table').on('click', '.js-delete', function() {
            var $btn = $(this);

            bootbox.setDefaults({locale: 'da'});
            bootbox.confirm('Er du sikker på at du vil slette siden?', function(result) {
                if (result) window.location.href = $btn.data('url');
            });
        });
    })();
</script>
@stop