<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('page_media', function(Blueprint $table)
		{
			$table->increments('id');
            $table->unsignedInteger('page_id')->index();
            $table->unsignedInteger('media_id')->index();
            $table->string('position', 40);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('page_media');
	}

}
