<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('links', function(Blueprint $table)
		{
			$table->increments('id');
            $table->unsignedInteger('linkable_id')->index();
            $table->string('linkable_type')->index();
            $table->tinyInteger('function_id')->unsigned();
            $table->unsignedInteger('page_id')->index();
            $table->unsignedInteger('news_id')->index();
            $table->string('url');
            $table->string('module');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('links');
	}

}
