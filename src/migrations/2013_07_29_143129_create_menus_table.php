<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menus', function(Blueprint $table)
		{
			$table->increments('id');
            $table->unsignedInteger('canonical_id')->nullable()->index();
            $table->unsignedInteger('language_id')->index();
            $table->string('name');
            $table->string('url_alias')->index();
            $table->unsignedInteger('parent_menu_id')->nullable()->index();
            $table->tinyInteger('level')->unsigned()->index();
            $table->boolean('active');
            $table->smallInteger('priority')->unsigned()->index()->default(1000);
            $table->unsignedInteger('views')->index();
            $table->boolean('locked');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menus');
	}

}
