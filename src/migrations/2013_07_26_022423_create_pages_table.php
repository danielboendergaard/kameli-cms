<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('language_id')->index();
            $table->unsignedInteger('canonical_id')->nullable()->index();
            $table->string('title', 100)->index();
            $table->string('url_alias', 120)->index();
            $table->string('meta_description');
            $table->text('short_description');

            $table->boolean('template');
            $table->enum('main_content_type', ['text', 'images', 'video']);
            $table->text('main_content');
            $table->string('main_video', 80);

            $table->enum('right_content_type', ['text', 'images', 'video']);
            $table->text('right_content');
            $table->string('right_video', 80);


            $table->boolean('published')->index();
            $table->unsignedInteger('category_id')->index();

            $table->unsignedInteger('revision');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
