<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news', function(Blueprint $table)
		{
			$table->increments('id');

            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('language_id')->index();
            $table->unsignedInteger('canonical_id')->nullable()->index();
            $table->string('title')->index();
            $table->string('slug')->index();
            $table->string('url_alias')->index();
            $table->string('meta_description');

            $table->text('short_description');
            $table->text('content');

            $table->boolean('public')->index();
            $table->boolean('publish_on')->index();
            $table->timestamp('publish')->nullable()->index();
            $table->boolean('unpublish_on')->index();
            $table->timestamp('unpublish')->nullable()->index();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news');
	}

}
