<?php namespace Kameli\Cms;

use Controller, View;

class CmsController extends Controller {

    /**
     * Show the CMS dashboard
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return View::make('kameli/cms::layout.base');
    }
}