<?php namespace Kameli\Cms;

use Kameli\Cms\Repositories\LanguageRepository;
use View, Input, Redirect, Controller;

class LanguageController extends Controller {

    private $languages;

    public function __construct(LanguageRepository $languages)
    {
        $this->languages = $languages;
    }

    /**
     * Show the available languages
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $languages = $this->languages->available();

        return View::make('kameli/cms::language.index', compact('languages'));
    }

    /**
     * Update the active languages
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update()
    {
        foreach (Input::get('language') as $id => $active)
        {
            if ($id == 1) continue;

            $this->languages->update($id, $active);
        }

        return Redirect::refresh()->with('success', 'Sproginstillingerne er gemt');
    }
} 