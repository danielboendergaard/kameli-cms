<?php namespace Kameli\Cms;

use App, Controller, Input, View, Request, Redirect;
use Kameli\Cms\Repositories\MediaRepository;

class MediaController extends Controller {

    private $media;

    public function __construct(MediaRepository $media)
    {
        $this->media = $media;
    }

    /**
     * Show the media in the specified folder
     * @param string $dir1
     * @param string $dir2
     * @param string $dir3
     * @param string $dir4
     * @param string $dir5
     * @return View
     */
    public function index($dir1 = null, $dir2 = null, $dir3 = null, $dir4 = null, $dir5 = null)
    {
        $path = $this->parseRelativePath(func_get_args());

        $files = $this->media->findByPath($path);

        if (func_num_args())
        {
            $segments = Request::segments();

            array_pop($segments);

            $backPath = implode('/', $segments);
        }

        $directories =  $this->media->getDirectories($path);

        return View::make('kameli/cms::media.index', compact('directories', 'backPath', 'files'));
    }

    /**
     * Upload a new file to the media manager
     * @param string $dir1
     * @param string $dir2
     * @param string $dir3
     * @param string $dir4
     * @param string $dir5
     * @return Redirect
     */
    public function store($dir1 = null, $dir2 = null, $dir3 = null, $dir4 = null, $dir5 = null)
    {
        if (Input::has('new_folder') and Input::get('folder'))
        {
            $this->media->createDirectory(Input::get('folder'), $this->parseRelativePath(func_get_args()));

            return Redirect::refresh();
        }

        $this->media->storeFiles(Input::file('files'), $this->parseRelativePath(func_get_args()));

        return Redirect::refresh();
    }

    /**
     * Delete a media file
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->media->delete($id);

        return Redirect::back();
    }

    /**
     * Parse the path from the function arguments
     * @param array $arguments
     * @return string
     */
    protected function parseRelativePath($arguments)
    {
        $path = implode('/', $arguments);

        if ( ! is_dir(public_path('media/orig/'.$path)))
        {
            App::abort(404);
        }

        return $path;
    }
}