<?php namespace Kameli\Cms;

use Kameli\Foundation\Exceptions\ResourceHasRelationException;
use CmsSentry, View, Input, Redirect, Controller;
use Illuminate\Support\MessageBag;
use Kameli\Cms\Repositories\PageRepository;
use Kameli\Cms\Repositories\LanguageRepository;
use Kameli\Cms\Repositories\CategoryRepository;
use Kameli\Cms\Services\Validators\PageValidator;

class PageController extends Controller {

    /**
     * @var PageRepository
     */
    private $pages;

    /**
     * @var LanguageRepository
     */
    private $languages;

    /**
     * @var CategoryRepository
     */
    private $categories;

    /**
     * Create the controller instance
     * @param PageRepository $pages
     * @param LanguageRepository $languages
     * @param CategoryRepository $categories
     */
    public function __construct(PageRepository $pages, LanguageRepository $languages, CategoryRepository $categories)
    {
        $this->pages = $pages;
        $this->languages = $languages;
        $this->categories = $categories;
    }

    /**
     * Show all pages
     * @return View
     */
    public function index()
    {
        $pages = $this->pages->all();

        return View::make('kameli/cms::page.index', compact('pages'));
    }

    /**
     * Create a new page
     * @return View
     */
    public function create()
    {
        $languages = $this->languages->visible();

        $categories = $this->categories->getPageCategories()->lists('name', 'id');

        return View::make('kameli/cms::page.create', compact('languages', 'categories'));
    }

    /**
     * Save a new page
     * @return Redirect
     */
    public function store()
    {
        // Check if the user has changed template usage
        if (Input::get('template') != Input::get('using_template'))
        {
            return Redirect::route('cms.page.create')->withInput();
        }

        $languages = $this->languages->visible();

        // Validate the input
        if ($errors = $this->pages->validate($languages, Input::all()))
        {
            return Redirect::route('cms.page.create')->withInput()->with('lang_errors', $errors);
        }

        // Create the pages
        $canonicalId = $this->pages->create($languages, Input::all(), CmsSentry::getUser()->getId());

        if (Input::get('_save-and-stay')) return Redirect::route('cms.page.edit', $canonicalId);

        return Redirect::route('cms.page.index');
    }

    /**
     * Edit a page
     * @param $page
     * @return View
     */
    public function edit($page)
    {
        $pages = $page->languageArray();

        $languages = $this->languages->visible();

        $defaultLanguage = array_keys($pages)[0];

        $categories = $this->categories->getPageCategories()->lists('name', 'id');

        return View::make('kameli/cms::page.edit', compact('page', 'pages', 'languages', 'defaultLanguage', 'categories'));
    }

    /**
     * Update a page
     * @param $page
     * @return Redirect
     */
    public function update($page)
    {
        // Check if the user has changed template usage
        if (Input::get('template') != Input::get('using_template'))
        {
            return Redirect::back()->withInput();
        }

        $languages = $this->languages->visible();

        // Validate the input
        if ($errors = $this->pages->validate($languages, Input::all()))
        {
            return Redirect::route('cms.page.edit', $page->id)->withInput()->with('lang_errors', $errors);
        }

        // Update the pages
        $this->pages->update($page->id, $languages, Input::all());

        if (Input::get('_save-and-stay')) return Redirect::route('cms.page.edit', $page->id);

        return Redirect::route('cms.page.index');
    }

    /**
     * Delete a page
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try
        {
            $this->pages->delete($id);
        }

        catch (ResourceHasRelationException $e)
        {
            $related = $e->getRelated()->groupBy(function($item)
            {
                return $item->linkable->getTypeName();
            });

            return Redirect::back()->with('related', $related);
        }

        return Redirect::back();
    }
}