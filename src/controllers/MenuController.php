<?php namespace Kameli\Cms;

use Config;
use Cookie;
use Kameli\Cms\Repositories\ModuleRepository;
use Kameli\Cms\Repositories\NewsRepository;
use Response;
use View, Input, Controller, Redirect;
use Kameli\Cms\Repositories\MenuRepository;
use Kameli\Cms\Repositories\PageRepository;
use Kameli\Cms\Repositories\LanguageRepository;

class MenuController extends Controller {

    private $menus;
    private $pages;
    private $languages;
    private $modules;

    /**
     * Create the controller instance
     * @param MenuRepository $menu
     * @param PageRepository $pages
     * @param NewsRepository $news
     * @param ModuleRepository $modules
     * @param LanguageRepository $languages
     */
    public function __construct(MenuRepository $menu,
                                PageRepository $pages,
                                NewsRepository $news,
                                ModuleRepository $modules,
                                LanguageRepository $languages)
    {
        $this->menus = $menu;
        $this->pages = $pages;
        $this->news = $news;
        $this->languages = $languages;
        $this->modules = $modules;
    }

    /**
     * Show the menu items in the top menu
     * @return View
     */
    public function index()
    {
        $menus = $this->menus->topLevelMenu();

        return View::make('kameli/cms::menu.index')
            ->with('menus', $menus)
            ->with('max_menu_levels', Config::get('kameli/cms::max_menu_levels'));
    }

    /**
     * Create a new menu item
     * @return View
     */
    public function create()
    {
        $languages = $this->languages->visible();

        $menus = $this->menus->parentMenuOptionList();

        $_functions = $this->menus->functions();
        $_pages = $this->pages->localizedOptionListWithHiddenInfo();
        $_news = $this->news->localizedOptionList('name_da');
        $_modules = $this->modules->getModules();

        return View::make('kameli/cms::menu.create', compact('languages', 'menus', '_functions', '_pages', '_news', '_modules'));
    }

    /**
     * Save a new menu item
     * @return Redirect
     */
    public function store()
    {
        $languages = $this->languages->visible();

        // Validate the input
        if ($errors = $this->menus->validate($languages, Input::all()))
        {
            return Redirect::route('cms.menu.create')->withInput()->with('lang_errors', $errors);
        }

        // Save the menus
        $this->menus->create($languages, Input::all());

        return Redirect::route('cms.menu.index');
    }

    /**
     * Edit a menu item
     * @param $item
     * @return View
     */
    public function edit($item)
    {
        $items = $item->languageArray();

        $languages = $this->languages->visible();

        $_functions = $this->menus->functions();
        $_pages = $this->pages->localizedOptionListWithHiddenInfo();
        $_news = $this->news->localizedOptionList('name_da');

        $_modules = $this->modules->getModules();

        return View::make('kameli/cms::menu.edit', compact('item', 'items', 'languages', '_functions', '_pages', '_news', '_modules'));
    }

    /**
     * Update a menu item
     * @param $item
     * @return Redirect
     */
    public function update($item)
    {
        $languages = $this->languages->visible();

        // Validate the input
        if ($errors = $this->menus->validate($languages, Input::all()))
        {
            return Redirect::route('cms.menu.edit', $item->id)->withInput()->with('lang_errors', $errors);
        }

        // Update the menus
        $this->menus->update($item->id, $languages, Input::all());

        return Redirect::route('cms.menu.index');
    }

    /**
     * Delete a menu item
     * @param int $id
     * @return Redirect
     */
    public function destroy($id)
    {
        $this->menus->delete($id);

        return Redirect::route('cms.menu.index');
    }

    /**
     * Update the order of the menu items
     */
    public function updateOrder()
    {
        $parentId = intval(Input::get('parentId')) ? intval(Input::get('parentId')) : null;

        $order = Input::get('order');

        $this->menus->updateMenuPosition($parentId, $order);

        return Response::make('', 200);
    }

    /**
     * Save the expanded status for the menu sections
     * @return Response
     */
    public function storeSectionStatus()
    {
        $name = Input::get('page');

        $cookie = Cookie::forever('menu_'.md5($name), Input::get('value'));

        return Response::make('')->withCookie($cookie);
    }
} 