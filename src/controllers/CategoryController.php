<?php namespace Kameli\Cms;

use Controller, Input;
use Kameli\Cms\Repositories\CategoryRepository;
use Kameli\Foundation\Exceptions\ResourceHasRelationException;
use Kameli\Foundation\Exceptions\ValidationException;
use Redirect;
use Request;
use View;

class CategoryController extends Controller {

    private $categories;

    public function __construct(CategoryRepository $categories)
    {
        $this->categories = $categories;
    }

    /**
     * Show all categories
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return View::make('kameli/cms::category.index')
            ->with('categories', $this->categories->getPageCategories());
    }

    /**
     * Save a new category
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        try
        {
            $category = $this->categories->create(Input::all());
        }

        catch (ValidationException $e)
        {
            if (Request::ajax()) return ['status' => 'error', 'message' => $e->getFirstError()];

            return Redirect::route('cms.category.index')->withInput()->withErrors($e);
        }

        if (Request::ajax()) return $category;

        return Redirect::route('cms.category.index');
    }

    /**
     * Show the edit form for a category
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $category = $this->categories->findByIdOrFail($id);

        return View::make('kameli/cms::category.edit', compact('category'));
    }

    /**
     * Update a category
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        try
        {
            $this->categories->update($id, Input::all());
        }

        catch (ValidationException $e)
        {
            return Redirect::route('cms.category.edit', $id)->withInput()->withErrors($e);
        }

        return Redirect::route('cms.category.index');
    }

    /**
     * Delete a category
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try
        {
            $this->categories->delete($id);
        }

        catch (ResourceHasRelationException $e)
        {
            return Redirect::route('cms.category.index')->with('error', $e->getMessage());
        }

        return Redirect::route('cms.category.index');
    }
} 