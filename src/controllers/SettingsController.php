<?php namespace Kameli\Cms;

use Controller, KameliCms, Redirect, Validator, Input, View;
use Kameli\Cms\Services\TrackingService;

class SettingsController extends Controller {

    /**
     * @var Services\TrackingService
     */
    protected $tracking;

    public function __construct(TrackingService $tracking)
    {
        $this->tracking = $tracking;
    }

    /**
     * Show the current settings
     * @return View
     */
    public function index()
    {
        return View::make('kameli/cms::settings.index')
            ->with('settings', KameliCms::getSettings())
            ->with('trackingTypes', $this->tracking->getTrackingTypes());
    }

    /**
     * Save the settings
     * @return Redirect
     */
    public function update()
    {
        $v = Validator::make(Input::all(), [
            'online' => ['in:0,1'],
            'index' => ['in:0,1'],
            'tracking_type' => ['in:0,1,2'],
        ]);

        if ($v->fails()) return Redirect::refresh()->withErrors(['Fejl i formularværdier']);

        KameliCms::saveSettings(Input::all());

        return Redirect::refresh()->with('success', 'Indstillingerne er gemt');
    }
} 