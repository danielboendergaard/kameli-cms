<?php namespace Kameli\Cms;

use Carbon\Carbon;
use CmsSentry, View, Input, Redirect, Controller;
use Illuminate\Support\MessageBag;
use Kameli\Cms\Repositories\NewsRepository;
use Kameli\Cms\Repositories\LanguageRepository;
use Kameli\Cms\Repositories\CategoryRepository;
use Kameli\Cms\Services\Validators\PageValidator;
use Kameli\Foundation\Exceptions\ResourceHasRelationException;

class NewsController extends Controller {

    private $languages;

    /**
     * Create the controller instance
     * @param NewsRepository $news
     * @param LanguageRepository $languages
     */
    public function __construct(NewsRepository $news, LanguageRepository $languages)
    {
        $this->news = $news;
        $this->languages = $languages;
    }

    /**
     * Show all news
     * @return View
     */
    public function index()
    {
        $news = $this->news->all();

        return View::make('kameli/cms::news.index', compact('news'));
    }

    /**
     * Create a new page
     * @return View
     */
    public function create()
    {
        $languages = $this->languages->visible();

        $now = Carbon::now()->format('d-m-Y H:i:s');

        return View::make('kameli/cms::news.create', compact('languages', 'now'));
    }

    /**
     * Save a new page
     * @return Redirect
     */
    public function store()
    {
        $languages = $this->languages->visible();

        // Validate the input
        if ($errors = $this->news->validate($languages, Input::all()))
        {
            return Redirect::route('cms.news.create')->withInput()->with('lang_errors', $errors);
        }

        // Create the pages
        $canonicalId = $this->news->create($languages, Input::all(), CmsSentry::getUser()->getId());

        if (Input::get('_save-and-stay')) return Redirect::route('cms.news.edit', $canonicalId);

        return Redirect::route('cms.news.index');
    }

    /**
     * Edit a page
     * @param int $id
     * @return View
     */
    public function edit($id)
    {
        $entry = $this->news->findById($id);

        $news = $entry->languageArray();

        $languages = $this->languages->visible();

        $defaultLanguage = array_keys($news)[0];

        return View::make('kameli/cms::news.edit', compact('entry', 'news', 'languages', 'defaultLanguage'));
    }

    /**
     * Update a news entry
     * @param int $id
     * @return Redirect
     */
    public function update($id)
    {
        $news = $this->news->findById($id);

        $languages = $this->languages->visible();

        // Validate the input
        if ($errors = $this->news->validate($languages, Input::all()))
        {
            return Redirect::route('cms.news.edit', $news->id)->withInput()->with('lang_errors', $errors);
        }

        // Update the pages
        $this->news->update($news->id, $languages, Input::all());

        if (Input::get('_save-and-stay')) return Redirect::route('cms.news.edit', $news->id);

        return Redirect::route('cms.news.index');
    }

    /**
     * Delete a news entry
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try
        {
            $this->news->delete($id);
        }

        catch (ResourceHasRelationException $e)
        {
            $related = $e->getRelated()->groupBy(function($item)
            {
                return $item->linkable->getTypeName();
            });

            return Redirect::back()->with('related', $related);
        }

        return Redirect::back();
    }
}