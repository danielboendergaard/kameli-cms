<?php namespace Kameli\Cms;

use Controller, Input, View, Redirect;
use Cartalyst\Sentry\Users\Eloquent\User;
use Kameli\Cms\Repositories\PermissionRepository;
use Kameli\Cms\Repositories\UserRepository;
use CmsSentry;
use Kameli\Foundation\Exceptions\ValidationException;

class UserController extends Controller {

    /**
     * @var UserRepository
     */
    private $users;

    /**
     * @var PermissionRepository
     */
    private $permissions;

    /**
     * Create the controller instance
     * @param UserRepository $users
     * @param PermissionRepository $permissions
     */
    public function __construct(UserRepository $users, PermissionRepository $permissions)
    {
        $this->users = $users;
        $this->permissions = $permissions;
    }

    public function index()
    {
        $users = $this->users->all();

        return View::make('kameli/cms::user.index', compact('users'));
    }

    public function create()
    {
        $permissions = $this->permissions->all();

        return View::make('kameli/cms::user.create', compact('permissions'));
    }

    public function store()
    {
        try
        {
            $this->users->create(Input::all());
        }

        catch (ValidationException $e)
        {
            return Redirect::route('cms.user.create')->withInput()->withErrors($e);
        }

        return Redirect::route('cms.user.index');
    }

    public function edit($id)
    {
        $user = $this->users->findById($id);

        $permissions = $this->permissions->all();

        return View::make('kameli/cms::user.edit', compact('user', 'permissions'));
    }

    /**
     * @param $id
     * @return Redirect
     */
    public function update($id)
    {
        try
        {
            $this->users->update($id, Input::all());
        }

        catch (ValidationException $e)
        {
            return Redirect::route('cms.user.edit', $id)->withInput()->withErrors($e);
        }

        return Redirect::route('cms.user.index');
    }

    /**
     * @param $id
     * @return Redirect
     */
    public function destroy($id)
    {
        $this->users->delete($id);

        return Redirect::route('cms.user.index');
    }

    /**
     * Show the activation form
     * @param string $code
     * @return View
     */
    public function activation($code)
    {
        return View::make('kameli/cms::user.activate');
    }

    /**
     * Activate the user
     * @param string $code
     * @return Redirect
     */
    public function activate($code)
    {
        if ($error = $this->users->validateActivation($code, Input::all()))
        {
            return Redirect::route('cms.user.activation', $code)->withInput()->withErrors($error);
        }

        // Activate user
        $user = $this->users->activate($code, Input::all());

        // Log the user in
        $this->users->login($user);

        // Redirect to front page
        return Redirect::route('cms.index');
    }


    public function editCurrentUser()
    {
        $user = CmsSentry::getUser();

        return View::make('kameli/cms::user.edit-current', compact('user'));
    }

    /**
     * Show the form for editing the current user
     * @return Redirect
     */
    public function storeCurrentUser()
    {
        $userId = CmsSentry::getUser()->getId();

        $v = $this->users->validate(Input::all(), $userId);

        if ($v->fails()) return Redirect::route('cms.user.edit-user')->withInput()->withErrors($v);

        $this->users->update($userId, Input::all());

        return Redirect::route('cms.user.edit-user')->with('success', 'Dine brugeroplysninger er gemt!');
    }

    /**
     * Show the change password form
     * @return View
     */
    public function editCurrentUserPassword()
    {
        return View::make('kameli/cms::user.change-password');
    }

    /**
     * Change the password of a user
     * @return Redirect
     */
    public function storeCurrentUserPassword()
    {
        $v = $this->users->validatePasswordChange(CmsSentry::getUser(), Input::all());

        if ($v->any()) return Redirect::route('cms.user.edit-password')->withErrors($v);

        $this->users->changePassword(CmsSentry::getUser(), Input::get('new_password'));

        return Redirect::route('cms.user.edit-password')->with('success', 'Koden er ændret!');
    }
}