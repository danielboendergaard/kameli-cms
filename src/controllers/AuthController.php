<?php namespace Kameli\Cms;

use View, Input, CmsSentry, Redirect, Exception, Controller;

class AuthController extends Controller {

    /**
     * Log the user in
     * @return \Illuminate\View\View
     */
    public function getLogin()
    {
        return View::make('kameli/cms::auth.login');
    }

    /**
     * Check the login details
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postLogin()
    {
        try
        {
            CmsSentry::authenticate(Input::only(['email', 'password']));
        }

        catch (Exception $e)
        {
            return Redirect::route('cms.login')->withInput()->withErrors(['error' => 'Fejl i e-mail eller kode']);
        }

        return Redirect::intended(route('cms.index'));
    }

    /**
     * Log the user out
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogout()
    {
        CmsSentry::logout();

        return Redirect::route('cms.login');
    }
}