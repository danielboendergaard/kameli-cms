<?php

if ( ! function_exists('locale_url'))
{
    /**
     * Get the url of the current route in another locale
     * @param string $locale
     * @param string $url
     * @return string
     */
    function locale_url($locale, $url = null)
    {
        $currentUrl = is_null($url) ? app('request')->getPathInfo() : parse_url($url, PHP_URL_PATH);

        $currentLocale = app()->getLocale();

        if ($locale == $currentLocale) return $currentUrl;

        if (count(explode('/', trim($currentUrl, '/'))) == 1)
        {
            // Remove the locale from the url if present
            $currentUrl = preg_replace('#^/([a-z]{2}$)?#', null, $currentUrl);
        }
        else
        {
            // Remove the locale from the url if present
            $currentUrl = preg_replace('#^/([a-z]{2}/)?#', null, $currentUrl);
        }

        $defaultLocale = app('config')['kameli/cms::config.default_language'];

        if ($locale == $defaultLocale) return url($currentUrl);

        return url($locale . '/' . $currentUrl);
    }
}

if ( ! function_exists('array_old_input'))
{
    /**
     * Get the old input from an array
     * @param string $key
     * @param string $attribute
     * @param mixed $items
     * @return mixed
     */
    function array_old_input($key, $attribute, $items = null)
    {
        $name = $key.'.'.$attribute;

        $value = isset($items[$key]) ? object_get($items[$key], $attribute) : null;

        return app('request')->old($name, $value);
    }
}