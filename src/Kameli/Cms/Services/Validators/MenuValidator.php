<?php namespace Kameli\Cms\Services\Validators;

use Kameli\Cms\Services\Validators\Traits\LinkValidatorTrait;
use Kameli\Foundation\Services\Validator as BaseValidator;

class MenuValidator extends BaseValidator {

    use LinkValidatorTrait;

    public $messages = [
        'name.required' => 'Du skal indtaste en titel'
    ];

    /**
     * Get the validation rules
     * @param int $id
     * @return array
     */
    protected function rules($id = null)
    {
        $rules = [
            'name' => 'required'
        ];

        return $this->mergeFunctionRules($rules);
    }
}