<?php namespace Kameli\Cms\Services\Validators;

use Validator as BaseValidator;

class UserValidator {

    public static $validationMessages = [
        'first_name.required' => 'Du skal indtaste et fornavn',
        'last_name.required' => 'Du skal indtaste et efternavn',
        'email.email' => 'Den indtastede e-mail er ugyldig',
        'email.required' => 'Du skal indtaste en e-mail adresse',
        'email.unique' => 'Der findes allerede en bruger med den indtastede e-mail adresse',

        'new_password.required' => 'Du skal indtaste en ny adgangskode',
        'new_password.min' => 'Din nye adganskode skal være minimun :size tegn',
        'new_password.confirmed' => 'Du har ikke indtastet den samme nye kode to gange'
    ];

    /**
     * Validate the user
     * @param array $input
     * @param string|int $id
     * @return \Illuminate\Validation\Validator
     */
    public function validate($input, $id = null)
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['required', 'email', "unique:cms_users,email,$id"]
        ];

        return BaseValidator::make($input, $rules, static::$validationMessages);
    }

    /**
     * Validate a password change
     * @param array $input
     * @return \Illuminate\Validation\Validator
     */
    public function validatePasswordChange($input)
    {
        $rules = ['new_password' => ['required', 'min:6', 'confirmed']];

        return BaseValidator::make($input, $rules, static::$validationMessages);
    }
}