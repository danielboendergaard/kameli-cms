<?php namespace Kameli\Cms\Services\Validators;

use Kameli\Foundation\Services\Validator as BaseValidator;

class PageValidator extends BaseValidator {

    /**
     * Get the validation rules
     * @param int $id
     * @return array
     */
    protected function rules($id = null)
    {
        $rules = [
            'title' => 'required',
            'published' => 'in:0,1',
            'category_id' => ['required', 'exists:categories,id'],
            'template' => 'in:0,1'
        ];

        if ($this->attributes['template'])
        {
            $rules = array_merge($rules, [
                'left_content_type' => ['required', 'in:text,images,video']
            ]);
        }

        return $rules;
    }
}