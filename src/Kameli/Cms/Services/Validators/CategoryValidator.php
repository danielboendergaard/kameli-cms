<?php namespace Kameli\Cms\Services\Validators;

use Kameli\Foundation\Services\Validator as BaseValidator;

class CategoryValidator extends BaseValidator {

    /**
     * The custom attribute names
     * @var array
     */
    protected $attributeNames = [
        'name' => 'Kategori'
    ];

    /**
     * Get the validation rules
     * @param int $id
     * @return array
     */
    protected function rules($id = null)
    {
        return [
            'name' => ['required', 'unique:categories,name,'.$id]
        ];
    }
}