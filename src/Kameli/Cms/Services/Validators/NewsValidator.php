<?php namespace Kameli\Cms\Services\Validators;

use Kameli\Foundation\Services\Validator as BaseValidator;

class NewsValidator extends BaseValidator {

    /**
     * Get the validation rules
     * @param int $id
     * @return array
     */
    protected function rules($id = null)
    {
        return  [
            'title' => 'required',
            'public' => 'in:0,1',
            'publish' => ['required_with:publish_on', 'date_format:d-m-Y H:i:s'],
            'unpublish' => ['required_with:unpublish_on', 'date_format:d-m-Y H:i:s']
        ];
    }
}