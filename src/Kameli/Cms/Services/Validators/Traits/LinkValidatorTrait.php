<?php namespace Kameli\Cms\Services\Validators\Traits;

trait LinkValidatorTrait {

    /**
     * Merge the rules for function choice
     * @param array $rules
     * @return array mixed
     */
    protected function mergeFunctionRules($rules)
    {
        $this->messages = array_merge($this->messages, [
            'function_id.required' => 'Du skal vælge en funktion',
            'function_id.in' => 'Den valgte funktion er ugyldig',
            'page_id.required' => 'Du skal vælge en side',
            'page_id.exists' => 'Den valgte side findes ikke',
            'news_id.required' => 'Du skal vælge en nyhed',
            'news_id.exists' => 'Den valgte nyhed findes ikke',
            'url.required' => 'Du skal indtaste en URL',
            'url.url' => 'Den indtastede url er ugyldig',
            'module.required' => 'Du skal vælge et modul',
            'module.route' => 'Det valgte modul er ugyldigt'
        ]);

        $rules['function_id'] = ['required', 'in:1,2,3,4'];

        switch ($this->attributes['function_id'])
        {
            case 1: $rules['page_id'] = ['required', 'exists:pages,id']; break;
            case 2: $rules['news_id'] = ['required', 'exists:news,id']; break;
            case 3: $rules['url'] = ['required', 'url']; break;
            case 4: $rules['module'] = ['required', 'module']; break;
        }

        return $rules;
    }
}