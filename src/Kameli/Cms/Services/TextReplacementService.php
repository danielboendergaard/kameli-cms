<?php namespace Kameli\Cms\Services;

use Closure;
use Illuminate\Container\Container;

class TextReplacementService {

    /**
     * The registered text replacements
     * @var array
     */
    protected $replacements = [];

    /**
     * The cached text replacements
     * @var array
     */
    protected $cachedReplacements = [];

    /**
     * @var Container
     */
    protected $container;

    /**
     * Make a new text replacement instance
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Register a text replacement
     * @param string $text
     * @param $replacement
     */
    public function registerReplacement($text, $replacement)
    {
        if ($replacement instanceof Closure)
        {
            $this->replacements[e($text)] = $replacement;
        }
        elseif (is_string($replacement))
        {
            list($class, $method) = explode('@', $replacement);

            $container = $this->container;

            $this->replacements[e($text)] = function() use ($class, $method, $container)
            {
                $callable = array($container->make($class), $method);

                return call_user_func($callable);
            };
        }
    }

    /**
     * Prepare the text with replacements
     * @param string $text
     * @return mixed
     */
    public function prepare($text)
    {
        foreach (array_keys($this->replacements) as $search)
        {
            $positions = [];

            $offset = 0;

            while (false !== ($position = strpos($text, $search, $offset)))
            {
                $positions[] = $position;
                $offset = $position + 1;
            }

            if ($positions)
            {
                $positions = array_reverse($positions);

                $length = strlen($search);

                foreach ($positions as $position)
                {
                    $text = substr_replace($text, $this->getReplacement($search), $position, $length);
                }
            }
        }

        return $text;
    }

    /**
     * Get the replacement text
     * @param string $search
     * @return mixed
     */
    protected function getReplacement($search)
    {
        if (isset($this->cachedReplacements[$search])) return $this->cachedReplacements[$search];

        $value = $this->replacements[$search];

        return $this->cachedReplacements[$search] = $value instanceof Closure ? $value() : $value;
    }
}