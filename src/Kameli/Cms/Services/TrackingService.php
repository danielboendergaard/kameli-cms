<?php namespace Kameli\Cms\Services;

class TrackingService {

    protected $types = [
        0 => 'Ingen',
        1 => 'Google Analytics',
        2 => 'Google Tag Manager'
    ];

    public function getTrackingTypes()
    {
        return $this->types;
    }
} 