<?php namespace Kameli\Cms\Auth;

use Illuminate\Support\Facades\Facade;

class CmsSentry extends Facade {

    protected static function getFacadeAccessor() { return 'sentry.cms'; }
}