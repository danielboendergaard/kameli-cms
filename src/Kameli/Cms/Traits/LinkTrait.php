<?php namespace Kameli\Cms\Traits;

trait LinkTrait {

    /**
     * The link relation
     * @return \Kameli\Cms\Models\Link|null
     */
    public function link()
    {
        return $this->morphOne('Kameli\Cms\Models\Link', 'linkable');
    }

    public function getUrl()
    {
        if ($this->link) return $this->link->getUrl();

        return '';
    }

    public function updateLinkRelation($attributes)
    {
        if ( ! is_null($this->link))
        {
            $this->link->update($attributes);
        }
        else
        {
            $this->link()->create($attributes);
        }
    }

    public abstract function getTypeName();
    public abstract function getDisplayName();

    /*
    |--------------------------------------------------------------------------
    | Getters and for form population
    |--------------------------------------------------------------------------
    */

    public function getFunctionIdAttribute()
    {
        return $this->getLinkRelationAttribute('function_id');
    }

    public function getPageIdAttribute()
    {
        return $this->getLinkRelationAttribute('page_id');
    }

    public function getNewsIdAttribute()
    {
        return $this->getLinkRelationAttribute('news_id');
    }

    public function getModuleAttribute()
    {
        return $this->getLinkRelationAttribute('module');
    }

    public function getUrlAttribute()
    {
        return $this->getLinkRelationAttribute('url');
    }

    protected function getLinkRelationAttribute($attribute)
    {
        if (is_null($this->link)) return null;

        return $this->link->{$attribute};
    }
}