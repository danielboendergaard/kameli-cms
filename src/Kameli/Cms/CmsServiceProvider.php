<?php namespace Kameli\Cms;

use Kameli\Cms\Menu\Menu;
use Kameli\Cms\Menu\Section;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Kameli\Cms\Repositories\MediaRepository;

class CmsServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 */
	public function boot()
	{
		$this->package('kameli/cms', 'kameli/cms');

        // Setup CMS alias
        AliasLoader::getInstance()->alias('KameliCms', 'Kameli\Cms\Facades\Cms');

        // Setup CMS alias
        AliasLoader::getInstance()->alias('CmsSentry', 'Kameli\Cms\Auth\CmsSentry');

        // Include routes file
        require __DIR__ . '/../../routes.php';

        // Include filters file
        require __DIR__ . '/../../filters.php';

        // Include view composers file
        require __DIR__ . '/../../composers.php';

        // Register the CMS standard menu items
        $this->registerMenuItems($this->app['cms.backend.menu']);

        // Register validators
        $this->registerValidationRules();

        $this->app['view']->share('_siteName', $this->app['cms']->getWebsiteName());
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        // Register Sentry service provider
        $this->app->register('Kameli\Cms\Auth\CmsSentryServiceProvider');

        // Register the commands for the CMS
        $this->registerCommands();

        // Register the CMS
        $this->app['cms'] = $this->app->share(function()
        {
            return new Cms($this->app['config']['kameli/cms::config']);
        });

        // Register the menu
        $this->app['cms.backend.menu'] = $this->app->share(function()
        {
            return new Menu;
        });

        // Register the module repository as a singleton
        $this->app->singleton('Kameli\Cms\Repositories\ModuleRepository', 'Kameli\Cms\Repositories\ModuleRepository');

        // Register the Dynamic Text Service
        $this->app->singleton('Kameli\Cms\Services\TextReplacementService', function()
        {
            return new Services\TextReplacementService($this->app);
        });

        $this->app->singleton('Kameli\Cms\Repositories\MenuRepository', 'Kameli\Cms\Repositories\MenuRepository');
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

    /**
     * Register commands
     */
    protected function registerCommands()
    {
        $this->app['command.cms.install'] = $this->app->share(function()
        {
            return new Console\InstallCommand;
        });

        $this->app['command.cms.create-user'] = $this->app->share(function()
        {
            return new Console\CreateUserCommand;
        });

        $this->app['command.cms.seed'] = $this->app->share(function()
        {
            return new Console\SeedCommand;
        });

        $this->app['command.cms.create-folders'] = $this->app->share(function()
        {
            return new Console\CreateFoldersCommand;
        });

        $this->commands([
            'command.cms.install',
            'command.cms.create-user',
            'command.cms.seed',
            'command.cms.create-folders'
        ]);
    }

    /**
     * Register the standard menu items
     * @param Menu $menu
     */
    protected function registerMenuItems(Menu $menu)
    {
        $section = new Section('Sider og menuer');

        $section->addItem(2, 'Sider', route('cms.page.index'), 'fa fa-file-text');
        $section->addItem(4, 'Menupunkter', route('cms.menu.index'), 'fa fa-indent');

        $menu->addSection('pages_and_menus', $section);

        $section = new Section('Standardmoduler');

        $section->addItem(10, 'Nyheder', route('cms.news.index'), 'fa fa-rss');
        $section->addItem(20, 'Kategorier', route('cms.category.index'), 'fa fa-tags');
        $section->addItem(30, 'Brugere', route('cms.user.index'), 'fa fa-user');
        $section->addItem(40, 'Mediemanager', '#', 'fa fa-cloud-upload', ['data-toggle' => 'mediamanager']);
        $section->addItem(50, 'Indstillinger', route('cms.settings.index'), 'fa fa-cogs');

        $menu->addSection('standard', $section);

        $section = new Section('Ekstramoduler');

        $section->addItem(5, 'Sprog', route('cms.language.index'), 'fa fa-flag');

        $menu->addSection('extra', $section);
    }

    /**
     * Register custom validation rules
     */
    protected function registerValidationRules()
    {
        $this->app['validator']->extend('module', function ($attribute, $value, $parameters)
        {
            return $this->app->make('Kameli\Cms\Repositories\ModuleRepository')->has($value);
        });

        $this->app['validator']->extend('media', function ($attribute, $value, $parameters)
        {
            return (new MediaRepository())->exists($value);
        });
    }
}