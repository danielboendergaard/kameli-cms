<?php namespace Kameli\Cms;

use File;

class Cms {

    /**
     * @var array
     */
    protected $settings = [];

    /**
     * @var array
     */
    protected $config;

    /**
     * Instantiate the CMS
     * @param array $config
     */
    public function __construct($config)
    {
        if (file_exists($this->settingsFile()))
        {
            $this->settings = json_decode(File::get($this->settingsFile()), true);
        }

        $this->settings = array_merge($this->settings, $config);
    }

    /**
     * Get the website name
     * @return string
     */
    public function getWebsiteName()
    {
        return array_get($this->settings, 'website_name', 'Kameli CMS');
    }

    /**
     * Get the permissions of the CMS
     * @return array
     */
    public function getPermissions()
    {
        return array_get($this->settings, 'permissions', []);
    }

    /**
     * Register one or more permissions
     * @param string|array $permissions
     */
    public function registerPermission($permissions)
    {
        $this->settings['permissions'] = array_merge($this->getPermissions(), (array) $permissions);
    }

    /**
     * Check if the CMS support multiple languages
     * @return bool
     */
    public function isMultiLanguage()
    {
        return $this->settings['languages'];
    }

    /**
     * Check if the CMS is online
     * @return bool
     */
    public function isOnline()
    {
        return (bool) array_get($this->settings, 'online', true);
    }

    /**
     * Get the offline message of the CMS
     * @return string
     */
    public function getOfflineMessage()
    {
        return array_get($this->settings, 'offline_message', 'Temporarily unavailable');
    }

    /**
     * Check of the CMS should be indexed by search engines
     * @return bool
     */
    public function shouldBeIndexed()
    {
        return (bool) array_get($this->settings, 'index', true);
    }

    /**
     * Get the default language
     * @return string
     */
    public function getDefaultLanguage()
    {
        return $this->settings['default_language'];
    }

    /**
     * Get the tracking type
     * @return int
     */
    public function getTrackingType()
    {
        return array_get($this->settings, 'tracking_type', 0);
    }

    /**
     * Get the tracking id
     * @return string|null
     */
    public function getTrackingId()
    {
        return array_get($this->settings, 'tracking_id');
    }

    /**
     * Get the CMS settings
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Save the CMS settings
     * @param array $settings
     */
    public function saveSettings($settings)
    {
        $settings = array_only($settings, ['website_name', 'online', 'offline_message', 'index', 'tracking_type', 'tracking_id']);

        File::put($this->settingsFile(), json_encode($settings));
    }

    /**
     * Get the path to the settings file
     * @return string
     */
    protected function settingsFile()
    {
        return storage_path('settings/cms.json');
    }
}