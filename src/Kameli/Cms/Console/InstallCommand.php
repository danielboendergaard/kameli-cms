<?php namespace Kameli\Cms\Console;

use Cartalyst\Sentry\Groups\GroupNotFoundException;
use Illuminate\Console\Command;

class InstallCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cms:install';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Install the CMS';

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
        // Create folders
        $this->call('cms:create-folders');

        // Migrate CMS
        $this->call('migrate', ['--package' => 'kameli/cms']);

        // Publish assets
        $this->call('asset:publish', ['kameli/cms']);
	}
}