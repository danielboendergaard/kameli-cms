<?php namespace Kameli\Cms\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CreateUserCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cms:create-user';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create a user in the CMS';

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$email = $this->ask('E-mail?');

        $password = $this->secret('Password');

        $this->laravel['sentry.cms']->register([
            'email' => $email,
            'password' => $password,
        ], true);
	}
}