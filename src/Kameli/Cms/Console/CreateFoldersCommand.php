<?php namespace Kameli\Cms\Console;

use Illuminate\Console\Command;

class CreateFoldersCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cms:create-folders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create folders used by the cms';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $this->info('Setting up directories');

        $folders = [
            public_path('media'),
            public_path('media/orig'),
            public_path('media/large'),
            public_path('media/page'),
            public_path('media/tn'),
            public_path('uploads'),
            public_path('modules'),
            storage_path('settings'),
        ];

        foreach ($folders as $folder)
        {
            if ( ! is_dir($folder))
            {
                mkdir($folder);

                chmod($folder, 0777);

                $this->info('Created ' . $folder);
            }
            else
            {
                $this->info($folder .  ' already exists');
            }
        }
    }
}