<?php namespace Kameli\Cms\Console;

use DB;
use Illuminate\Console\Command;
use Kameli\Cms\Models\Category;

class SeedCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cms:seed';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Fill database with data';

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
        $this->seedLanguages();

        $this->seedCategories();
    }

    protected function seedLanguages()
    {
        $languages = [
            ['code' => 'DK', 'locale' => 'da', 'name_da' => 'Dansk', 'name_en' => 'Danish', 'available' => 1, 'active' => 1],
            ['code' => 'GB', 'locale' => 'en', 'name_da' => 'Engelsk', 'name_en' => 'English', 'available' => 0, 'active' => 0],
            ['code' => 'DE', 'locale' => 'de', 'name_da' => 'Tysk', 'name_en' => 'German', 'available' => 0, 'active' => 0],
            ['code' => 'FR', 'locale' => 'fr', 'name_da' => 'Fransk', 'name_en' => 'French', 'available' => 0, 'active' => 0],
            ['code' => 'IT', 'locale' => 'it', 'name_da' => 'Italiensk', 'name_en' => 'Italian', 'available' => 0, 'active' => 0],
            ['code' => 'ES', 'locale' => 'es', 'name_da' => 'Spansk', 'name_en' => 'Spanish', 'available' => 0, 'active' => 0],
            ['code' => 'PL', 'locale' => 'pl', 'name_da' => 'Polsk', 'name_en' => 'Polish', 'available' => 0, 'active' => 0],
            ['code' => 'RU', 'locale' => 'ru', 'name_da' => 'Russisk', 'name_en' => 'Russian', 'available' => 0, 'active' => 0],
            ['code' => 'RO', 'locale' => 'ro', 'name_da' => 'Rumænsk', 'name_en' => 'Romanian', 'available' => 0, 'active' => 0],
            ['code' => 'NL', 'locale' => 'nl', 'name_da' => 'Hollandsk', 'name_en' => 'Dutch', 'available' => 0, 'active' => 0],
            ['code' => 'HU', 'locale' => 'hu', 'name_da' => 'Ungarsk', 'name_en' => 'Hungarian', 'available' => 0, 'active' => 0],
            ['code' => 'PT', 'locale' => 'pt', 'name_da' => 'Portugisisk', 'name_en' => 'Portuguese', 'available' => 0, 'active' => 0],
            ['code' => 'CZ', 'locale' => 'cs', 'name_da' => 'Tjekkisk', 'name_en' => 'Czech', 'available' => 0, 'active' => 0],
            ['code' => 'SE', 'locale' => 'sv', 'name_da' => 'Svensk', 'name_en' => 'Swedish', 'available' => 0, 'active' => 0],
            ['code' => 'GR', 'locale' => 'el', 'name_da' => 'Græsk', 'name_en' => 'Greek', 'available' => 0, 'active' => 0],
            ['code' => 'BG', 'locale' => 'bg', 'name_da' => 'Bulgarsk', 'name_en' => 'Bulgarian', 'available' => 0, 'active' => 0],
            [ 'code' => 'SK', 'locale' => 'sk', 'name_da' => 'Slovakisk', 'name_en' => 'Slovak', 'available' => 0, 'active' => 0],
            ['code' => 'TR', 'locale' => 'tr', 'name_da' => 'Tyrkisk', 'name_en' => 'Turkish', 'available' => 0, 'active' => 0],
            ['code' => 'FI', 'locale' => 'fi', 'name_da' => 'Finsk', 'name_en' => 'Finnish', 'available' => 0, 'active' => 0],
            ['code' => 'CN', 'locale' => 'zh', 'name_da' => 'Kinesisk', 'name_en' => 'Chinese', 'available' => 0, 'active' => 0],
            ['code' => 'JP', 'locale' => 'ja', 'name_da' => 'Japansk', 'name_en' => 'Japanese', 'available' => 0, 'active' => 0],
            ['code' => 'NO', 'locale' => 'no', 'name_da' => 'Norsk', 'name_en' => 'Norwegian', 'available' => 0, 'active' => 0]
        ];

        DB::table('languages')->insert($languages);
    }

    protected function seedCategories()
    {
        Category::insert(['name' => 'Ingen']);
    }

}