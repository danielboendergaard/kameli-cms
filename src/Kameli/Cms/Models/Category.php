<?php namespace Kameli\Cms\Models;

use Eloquent;

class Category extends Eloquent {

    protected $table = 'categories';

    protected $fillable = ['name'];

    /**
     * The pages relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pages()
    {
        return $this->hasMany('Kameli\Cms\Models\Page', 'category_id');
    }

    public function publishedPages()
    {
        return $this->pages()->where('published', 1);
    }
} 