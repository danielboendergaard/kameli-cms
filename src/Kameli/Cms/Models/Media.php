<?php namespace Kameli\Cms\Models;

use Eloquent;

class Media extends Eloquent {

    protected $table = 'media';

    protected $guarded = [];

    protected $softDelete = true;

    public function thumbnail()
    {
        return 'media/tn/'.$this->path.'/'.$this->filename;
    }

    public function page()
    {
        return 'media/page/'.$this->path.'/'.$this->filename;
    }

    public function filePath($size)
    {
        return public_path('media/' . $size . '/' . $this->path . '/' . $this->filename);
    }

    public function assetPath()
    {
        return asset('media/orig/' . $this->path . '/' . $this->filename);
    }

    public function isImage()
    {
        $extension = substr(strrchr($this->filename, '.'), 1);

        return in_array($extension, ['jpg', 'jpeg', 'gif', 'png']);
    }
} 