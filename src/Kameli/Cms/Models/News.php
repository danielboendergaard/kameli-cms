<?php namespace Kameli\Cms\Models;

use Carbon\Carbon;
use DateTime;
use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Kameli\Foundation\Traits\EloquentSluggableTrait;

class News extends Eloquent {

    use EloquentSluggableTrait;

    protected $table = 'news';

    protected $guarded = [];

    public function translated()
    {
        return $this->hasMany('Kameli\Cms\Models\News', 'canonical_id');
    }

    public function publishedTranslated()
    {
        return $this->hasMany('Kameli\Cms\Models\News', 'canonical_id')->wherePublished();
    }

    public function language()
    {
        return $this->belongsTo('Kameli\Cms\Models\Language', 'language_id');
    }

    public function publishedLanguages()
    {
        $posts = new Collection;

        if ($this->isPublished()) $posts->add($this);

        $posts = $posts->merge($this->publishedTranslated);

        $posts->load('language');

        return $posts->fetch('language');
    }

    public function languageArray()
    {
        $posts = (new Collection)->add($this);

        $posts = $posts->merge($this->translated);

        $posts->load('language');

        $languages = [];

        foreach ($posts as $post)
        {
            $languages[$post->language->code] = $post;
        }

        return $languages;
    }

    public function setPublishAttribute($value)
    {
        $value = is_null($value) ? $value : Carbon::createFromFormat('d-m-Y H:i:s', $value)->toDateTimeString();

        $this->attributes['publish'] = $value;
    }

    public function getPublishAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d-m-Y H:i:s') : '';
    }

    public function setUnpublishAttribute($value)
    {
        $value = is_null($value) ? $value : Carbon::createFromFormat('d-m-Y H:i:s', $value)->toDateTimeString();

        $this->attributes['unpublish'] = $value;
    }

    public function getUnpublishAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d-m-Y H:i:s') : '';
    }

    public function getPublishedAtAttribute()
    {
        if ($this->publish_on) return $this->publish;

        return $this->created_at;
    }

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = array())
    {
        $this->setSlugged('slug', $this->title, $this->language_id);

        return parent::save($options);
    }

    /**
     * Get published status
     * @return bool
     */
    public function isPublished()
    {
        $timedPublish = $this->publish_on and ($this->publish > new DateTime);

        $timedUnpublish = ! $this->unpublish_on or ($this->unpublish < new DateTime);

        return ($this->public or $timedPublish) and $timedUnpublish;
    }

    public function scopeWherePublished($query)
    {
        return $query->whereNested(function($query)
        {
            $query->where('public', 1);

            $query->whereNested(function($query)
            {
                $query->where('publish_on', 1);
                $query->Where('publish', '>', new DateTime);
            }, 'or');

        })->whereNested(function($query)
        {
            $query->where('unpublish_on', 0);
            $query->OrWhere('unpublish', '<', new DateTime);
        });
}

    /**
     * Filter on specific locale
     * @param Builder $query
     * @param string $locale
     * @return Builder mixed
     */
    public function scopeWhereLocale($query, $locale)
    {
        return $query->join('languages', 'languages.id', '=', 'news.language_id')
            ->where('languages.locale', $locale)
            ->select($this->getTable() . '.*');
    }

}