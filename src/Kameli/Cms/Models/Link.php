<?php namespace Kameli\Cms\Models;

use Eloquent;

class Link extends Eloquent {

    const PAGE = 1;
    const NEWS = 2;
    const EXTERNAL = 3;
    const MODULE = 4;

    protected $table = 'links';

    protected $fillable = ['linkable_id', 'linkable_type', 'function_id', 'page_id', 'news_id', 'url', 'module'];

    public function linkable()
    {
        return $this->morphTo();
    }

    public function page()
    {
        return $this->belongsTo('Kameli\Cms\Models\Page', 'page_id');
    }

    public function news()
    {
        return $this->belongsTo('Kameli\Cms\Models\News', 'news_id');
    }

    /**
     * Get the url of the menu item
     * @return string
     */
    public function getUrl()
    {
        $modules = \App::make('Kameli\Cms\Repositories\ModuleRepository');

        if ($this->function_id == 1)
        {
            return route('page.show', $this->page->url_alias);
        }

        if ($this->function_id == 2)
        {
            return route('news.show', $this->news->slug);
        }

        // External URL
        if ($this->function_id == 3) return $this->url;

        if ($this->function_id == 4)
        {
            return $modules->getRoute($this->module);
        }

        return '';
    }
} 