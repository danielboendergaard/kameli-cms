<?php namespace Kameli\Cms\Models;

use Eloquent;

class Language extends Eloquent {

    protected $table = 'languages';
} 