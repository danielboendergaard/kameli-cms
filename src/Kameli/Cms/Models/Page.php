<?php namespace Kameli\Cms\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Page
 * @package Kameli\Cms
 *
 * @property Language $language
 * @property Category $category
 * @property Collection $images
 */
class Page extends Eloquent {

    protected $table = 'pages';

    protected $guarded = ['id', 'user_id', 'language_id', 'canonical_id', 'revision', 'created_at', 'updated_at'];

    public function translated()
    {
        return $this->hasMany('Kameli\Cms\Models\Page', 'canonical_id');
    }

    public function publishedTranslated()
    {
        return $this->hasMany('Kameli\Cms\Models\Page', 'canonical_id')->where('published', 1);
    }

    public function language()
    {
        return $this->belongsTo('Kameli\Cms\Models\Language', 'language_id');
    }

    public function category()
    {
        return $this->belongsTo('Kameli\Cms\Models\Category', 'category_id');
    }

    public function images()
    {
        return $this->belongsToMany('Kameli\Cms\Models\Media', 'page_media', 'page_id', 'media_id')->withPivot(['position']);
    }

    public function mainImages()
    {
        return $this->images->filter(function($item)
        {
            return $item->pivot->position == 'left';
        });
    }

    public function rightImages()
    {
        return $this->images->filter(function($item)
        {
            return $item->pivot->position == 'right';
        });
    }

    public function publishedLanguages()
    {
        $posts = new Collection;

        if ($this->published) $posts->add($this);

        $posts = $posts->merge($this->publishedTranslated);

        $posts->load('language');

        return $posts->fetch('language');
    }

    public function languageArray()
    {
        $posts = (new Collection)->add($this);

        $posts = $posts->merge($this->translated);

        $posts->load('language');

        $languages = [];

        foreach ($posts as $post)
        {
            $languages[$post->language->code] = $post;
        }

        return $languages;
    }

    /**
     * Filter on specific locale
     * @param Builder $query
     * @param string $locale
     * @return Builder mixed
     */
    public function scopeWhereLocale($query, $locale)
    {
        return $query->join('languages', 'languages.id', '=', $this->getTable() . '.language_id')
            ->where('languages.locale', $locale)
            ->select($this->getTable() . '.*');
    }

    /*
    |--------------------------------------------------------------------------
    | Getters and Setters
    |--------------------------------------------------------------------------
    */

    /**
     * Set the URL Alias
     * @param string $alias
     * @param int $language_id
     */
    public function setUrlAlias($alias, $language_id)
    {
        while ($this->causesUrlAliasCollision($alias, $language_id, $this->exists))
        {
            if (preg_match('/\d+$/', $alias, $matches))
            {
                $alias = preg_replace('/\d+$/', ++$matches[0], $alias);
            }
            else
            {
                $alias .= '-1';
            }
        }

        $this['url_alias'] = $alias;
    }

    /**
     * Check if the url alias causes collisions in the DB
     * @param string $alias
     * @param int $languageId
     * @param bool $exists
     * @return bool
     */
    protected function causesUrlAliasCollision($alias, $languageId, $exists = false)
    {
        $query = static::where('url_alias', '!=', '')->where('url_alias', $alias)->where('language_id', $languageId);

        if ($exists) $query->where('id', '!=', $this->id);

        return (bool) $query->count();
    }

    public function setMainVideoAttribute($value)
    {
        $this->attributes['main_video'] = $this->parseYoutubeVideoUrl($value);
    }

    public function setRightVideoAttribute($value)
    {
        $this->attributes['right_video'] = $this->parseYoutubeVideoUrl($value);
    }

    protected function parseYoutubeVideoUrl($url)
    {
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {

            return 'http://www.youtube.com/embed/' . $match[1] . '?rel=0';
        }

        return '';
    }
}