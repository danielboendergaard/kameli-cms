<?php namespace Kameli\Cms\Models;

use Cartalyst\Sentry\Groups\Eloquent\Group;

class CmsGroup extends Group {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_groups';
} 