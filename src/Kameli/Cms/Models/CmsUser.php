<?php namespace Kameli\Cms\Models;

use Cartalyst\Sentry\Users\Eloquent\User;

class CmsUser extends User {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_users';

    /**
     * Returns the relationship between users and groups.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(static::$groupModel, static::$userGroupsPivot, 'user_id', 'group_id');
    }
}