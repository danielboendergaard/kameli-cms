<?php namespace Kameli\Cms\Models;

use Eloquent;

class Upload extends Eloquent {

    protected $table = 'uploads';

    protected $guarded = [];

    protected $softDelete = true;

    public function getPath()
    {
        return public_path('uploads/' . $this->path . '/' . $this->filename);
    }

    public function getPublicPath()
    {
        $path = $this->path ? $this->path . '/' : '';

        return asset('uploads/' . $path . $this->filename);
    }

    public function getIconClass()
    {
        $extension = substr(strrchr($this->filename, '.'), 1);

        switch ($extension)
        {
            case 'pdf': return 'fa fa-file';
            case 'zip':
            case 'rar':
                return 'fa fa-dropbox';
            case 'doc':
            case 'docx':
                return 'fa fa-file-text';
            case 'xls':
            case 'xlsx':
                return 'fa fa-table';
        };

        return 'fa fa-question';
    }
} 