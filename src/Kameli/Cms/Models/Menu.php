<?php namespace Kameli\Cms\Models;

use Eloquent, URL;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Kameli\Cms\Traits\LinkTrait;
use Log;
use Request;

/**
 * Class Menu
 * @package Kameli\Cms\Models
 *
 * @property Collection $subMenus
 * @property Page $page
 */
class Menu extends Eloquent {

    use LinkTrait;

    protected $table = 'menus';

    protected $guarded = [];

    protected $with = ['link'];

    public function translated()
    {
        return $this->hasMany('Kameli\Cms\Models\Menu', 'canonical_id');
    }

    public function activeTranslatedGroupedByLocale()
    {
        $translated = $this->hasMany('Kameli\Cms\Models\Menu', 'canonical_id')->where('active', 1)->get();

        $grouped = [];

        foreach ($translated as $trans)
        {
            $grouped[$trans->language->locale] = $trans;
        }

        return $grouped;
    }

    public function language()
    {
        return $this->belongsTo('Kameli\Cms\Models\Language', 'language_id');
    }

    public function parentMenu()
    {
        return $this->belongsTo('Kameli\Cms\Models\Menu', 'parent_menu_id');
    }

    public function subMenus()
    {
        return $this->hasMany('Kameli\Cms\Models\Menu', 'parent_menu_id')->orderBy('priority');
    }

    public function render()
    {
        return '<a href="' . $this->getUrl() .'"'. $this->getTarget() . '>' . $this->name . '</a>';
    }

    public function getActiveSubMenus()
    {
        return $this->subMenus->filter(function($item)
        {
            return $item->active;
        });
    }

    /**
     * Get the url of the menu item
     * @return string
     */
    public function getUrl()
    {
        $modules = \App::make('Kameli\Cms\Repositories\ModuleRepository');

        if ($this->function_id == 1)
        {
            return route('slug', [$this->url_alias, $this->id]);
        }

        if ($this->function_id == 2)
        {
            return route('news.show', $this->link->news->slug);
        }

        // External URL
        if ($this->function_id == 3) return $this->link->url;

        if ($this->function_id == 4)
        {
            return $modules->getRoute($this->module, [$this->id]);
        }

        return '';
    }

    /**
     * Get the target of the link
     * @return string
     */
    public function getTarget()
    {
        if ($this->function_id == 3) return ' target="_blank"';

        return '';
    }

    public function isActive()
    {
        if (URL::current() == $this->getUrl()) return true;

        foreach ($this->getActiveSubMenus() as $subMenu)
        {
            if ($subMenu->getUrl() == URL::current()) return true;
        }

        return false;
    }

    /**
     * Get the class for the html element
     * @return string
     */
    public function getClass()
    {
        return $this->isActive() ? 'active' : '';

        if ($menuLevel == count(Request::segments()))
        {
            return (rtrim(URL::current(), '/') == rtrim($this->getUrl(), '/')) ? ' active' : '';
        }

        $menuSegment = array_slice(explode('/', $this->getUrl()), 3)[0];

        return $menuSegment == Request::segment($menuLevel) ? ' active' : '';
    }

    /**
     * Get the canonical url
     * @return string
     */
    public function getCanonical()
    {
        return '';
        // find menu item with same page, get most views and return
    }

    /**
     * Get the active languages of the menu item
     * @return Collection
     */
    public function activeLanguages()
    {
        $menus = new Collection;

        if ($this->active) $menus->add($this);

        $menus = $menus->merge($this->translated()->where('active', 1)->get());

        $menus->load('language');

        return $menus->fetch('language');
    }

    public function languageArray()
    {
        $items = (new Collection)->add($this);

        $items = $items->merge($this->translated);

        $items->load('language');

        $languages = [];

        foreach ($items as $item)
        {
            $languages[$item->language->code] = $item;
        }

        return $languages;
    }

    /*
    |--------------------------------------------------------------------------
    | Getters and Setters
    |--------------------------------------------------------------------------
    */

    protected function setPageIdAttribute($value)
    {
        $this->attributes['page_id'] = $value ? $value : null;
    }

    protected function setParentMenuIdAttribute($value)
    {
        $this->attributes['parent_menu_id'] = $value ? $value : null;
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeSort($query)
    {
        return $query->orderBy('priority');
    }

    /**
     * Set the URL Alias
     * @param string $alias
     * @param int $language_id
     */
    public function setUrlAlias($alias, $language_id)
    {
        if ($this->locked) return;

        while ($this->causesUrlAliasCollision($alias, $language_id, $this->exists))
        {
            if (preg_match('/\d+$/', $alias, $matches))
            {
                $alias = preg_replace('/\d+$/', ++$matches[0], $alias);
            }
            else
            {
                $alias .= '-1';
            }
        }

        $this['url_alias'] = $alias;
    }

    /**
     * Check if the url alias causes collisions in the DB
     * @param string $alias
     * @param int $languageId
     * @param bool $exists
     * @return bool
     */
    protected function causesUrlAliasCollision($alias, $languageId, $exists = false)
    {
        $query = static::where('url_alias', '!=', '')->where('url_alias', $alias)->where('language_id', $languageId);

        if ($exists) $query->where('id', '!=', $this->id);

        return (bool) $query->count();
    }

    /**
     * Delete the menu item
     * @return void
     */
    public function delete()
    {
        $this->link()->delete();

        parent::delete();
    }

    /**
     * Filter on specific locale
     * @param Builder $query
     * @param string $locale
     * @return Builder mixed
     */
    public function scopeWhereLocale($query, $locale)
    {
        return $query->join('languages', 'languages.id', '=', $this->getTable() . '.language_id')
            ->where('languages.locale', $locale)
            ->select($this->getTable() . '.*');
    }

    /**
     * Filter on specific locale
     * @param Builder $query
     * @param string $column
     * @param string $operator
     * @param mixed $value
     * @param string $boolean
     * @return Builder mixed
     */
    public function scopeWhereTable($query, $column, $operator = null, $value = null, $boolean = 'and')
    {
        return $query->where($this->getTable().'.'.$column, $operator, $value, $boolean);
    }

    public function getTypeName()
    {
        return 'Menupunkter';
    }

    public function getDisplayName()
    {
        return $this->name;
    }
}