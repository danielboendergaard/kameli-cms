<?php namespace Kameli\Cms\Repositories;

use Kameli\Cms\Models\Category;
use Kameli\Cms\Services\Validators\CategoryValidator;
use Kameli\Foundation\Exceptions\ResourceHasRelationException;
use Kameli\Foundation\Exceptions\ValidationException;

class CategoryRepository {

    private $validator;

    public function __construct(CategoryValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Get all categories
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return Category::all();
    }

    /**
     * Find by id or throw a 404
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByIdOrFail($id)
    {
        return Category::findOrFail($id);
    }

    /**
     * Get all categories for pages
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getPageCategories()
    {
        $collection = Category::orderBy('name')->get();

        $key = array_search(1, $collection->modelKeys());

        // Take the entry with id = 1
        $item = $collection->get($key);

        // Delete it
        $collection->forget($key);

        // Re-add it at the start of the collection
        $collection->push($item);

        return $collection;
    }

    protected function validate($attributes, $id = null)
    {
        if ($this->validator->with($attributes, $id)->fails())
        {
            throw new ValidationException($this->validator);
        }
    }

    /**
     * Create a new category
     * @param $attributes
     * @return Category|false
     */
    public function create($attributes)
    {
        $this->validate($attributes);

        return Category::create(array_only($attributes, 'name'));
    }

    /**
     * Update a category
     * @param int $id
     * @param $attributes
     * @return Category
     */
    public function update($id, $attributes)
    {
        $this->validate($attributes);

        $category = Category::findOrFail($id);

        $category->update(array_only($attributes, 'name'));

        return $category;
    }

    /**
     * Delete a category
     * @param int $id
     * @throws \Kameli\Foundation\Exceptions\ResourceHasRelationException
     */
    public function delete($id)
    {
        $category = Category::findOrFail($id);

        if ($category->pages()->count())
        {
            throw new ResourceHasRelationException('Kategorien har sider tilknyttet og kan derfor ikke slettes');
        }

        $category->delete();
    }
}