<?php namespace Kameli\Cms\Repositories;

use Cartalyst\Sentry\Users\UserInterface;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\Validator;
use Kameli\Cms\Services\Validators\UserValidator;
use Kameli\Foundation\Exceptions\ValidationException;
use KameliCms;
use Mail;
use Str;
use CmsSentry;
use Cartalyst\Sentry\Users\Eloquent\User;

class UserRepository {

    /**
     * @var UserValidator
     */
    private $validator;

    public function __construct(UserValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Get all users
     * @return User[]
     */
    public function all()
    {
        return CmsSentry::getUserProvider()->findAll();
    }

    /**
     * Find a user by id
     * @param int $id
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     * @return User
     */
    public function findById($id)
    {
        try
        {
            return CmsSentry::getUserProvider()->findById($id);
        }

        catch (UserNotFoundException $e)
        {
            throw new ModelNotFoundException(null, null, $e);
        }
    }

    /**
     * Validate the user
     * @param array $input
     * @param int|string $id
     * @return Validator
     */
    public function validate($input, $id = 'null')
    {
        return $this->validator->validate($input, $id);
    }

    /**
     * Create a new user
     * @param array $attributes
     * @throws \Kameli\Foundation\Exceptions\ValidationException
     * @return User
     */
    public function create($attributes)
    {
        $v = $this->validator->validate($attributes);

        if ($v->fails()) throw new ValidationException($v);

        // Password is required, so put in a random one to start with
        $attributes['password'] = Str::random(32);

        $user = CmsSentry::createUser($attributes);

        $data = [
            'base_url' => url('/'),
            'activation_url' => route('cms.user.activation', $user->getActivationCode()),
            'name' => $user->first_name . ' ' . $user->last_name
        ];

        // Send activation mail
        Mail::queue('kameli/cms::emails.activation', $data, function($m) use ($user)
        {
            $m->to($user->email);
            $m->subject('Aktiver din bruger på ' . KameliCms::getWebsiteName());
        });

        return $user;
    }

    /**
     * Update the user
     * @param int $userId
     * @param array $attributes
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     * @throws \Kameli\Foundation\Exceptions\ValidationException
     */
    public function update($userId, $attributes)
    {
        $v = $this->validator->validate($attributes, $userId);

        if ($v->fails()) throw new ValidationException($v);

        try
        {
            /** @var User $user */
            $user = CmsSentry::findUserById($userId);
        }

        catch (UserNotFoundException $e)
        {
            throw new ModelNotFoundException(null, null, $e);
        }

        $user->update($attributes);
    }

    /**
     * Delete a user
     * @param int $id
     */
    public function delete($id)
    {
        $this->findById($id)->delete();
    }

    /**
     * Validate the activation info
     * @param string $code
     * @param array $input
     * @return null|string
     */
    public function validateActivation($code, $input)
    {
        if ( ! $user = CmsSentry::getUserProvider()->findByActivationCode($code))
        {
            return 'Aktiverigskoden er ugyldig';
        }

        if ($user->email != $input['email'])
        {
            return 'E-mail adressen stemmer ikke overens med aktiveringskoden';
        }

        if ($user->activated)
        {
            return 'Brugeren er allerede aktiveret';
        }

        if (strlen($input['password']) < 6)
        {
            return 'Dit kodeord skal være mindst 6 tegn';
        }

        if ($input['password'] != $input['password_confirmation'])
        {
            return 'De to kodeord skal være ens';
        }

        return null;
    }

    /**
     * Activate a user
     * @param string $code
     * @param array $input
     * @return User
     */
    public function activate($code, $input)
    {
        /** @var User $user */
        $user = CmsSentry::getUserProvider()->findByActivationCode($code);

        $user->attemptActivation($code);

        $user->password = $input['password'];
        $user->save();

        return $user;
    }

    /**
     * Log a user in
     * @param User $user
     */
    public function login($user)
    {
        CmsSentry::login($user);
    }

    /**
     * @param UserInterface $user
     * @param array $input
     * @return MessageBag
     */
    public function validatePasswordChange($user, $input)
    {
        $messages = new MessageBag;

        if ( ! $user->checkPassword(array_get($input, 'current_password')))
        {
            $messages->add('current_password', 'Dit nuværende password er ugyldigt');
        }

        $v = $this->validator->validatePasswordChange($input);

        if ($v->fails()) $messages->merge($v->errors()->toArray());

        return $messages;
    }

    /**
     * Change the password of a user
     * @param UserInterface $user
     * @param string $password
     */
    public function changePassword($user, $password)
    {
        $user->password = $password;
        $user->save();
    }
}