<?php namespace Kameli\Cms\Repositories;

use DB;
use KameliCms;
use Kameli\Cms\Models\Language;
use Illuminate\Database\Eloquent\Collection;

class LanguageRepository {

    /**
     * Find the id of the language from it's locale
     * @param string $locale
     * @return int
     */
    public function findIdFromLocale($locale)
    {
        return Language::where('locale', $locale)->pluck('id');
    }

    /**
     * Get  the visible languages
     * @return Collection
     */
    public function visible()
    {
        $default = KameliCms::getDefaultLanguage();

        $orderBy = DB::raw("(locale = '{$default}') desc, id");

        return Language::where('available', 1)->where('active', 1)->orderBy($orderBy)->get();
    }

    /**
     * Get the available languages
     * @return Collection
     */
    public function available()
    {
        $default = KameliCms::getDefaultLanguage();

        $orderBy = DB::raw("(locale = '{$default}') desc, id");

        return Language::where('available', 1)->orderBy($orderBy)->get();
    }

    /**
     * Update status of language
     * @param int $id
     * @param boolean $active
     */
    public function update($id, $active)
    {
        Language::where('id', $id)->update(['active' => $active]);
    }
} 