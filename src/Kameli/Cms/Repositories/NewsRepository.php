<?php namespace Kameli\Cms\Repositories;

use App;
use Illuminate\Database\Eloquent\Collection;
use Kameli\Cms\Models\Link;
use Kameli\Cms\Models\News;
use Kameli\Cms\Services\Validators\NewsValidator;
use Kameli\Foundation\Exceptions\ResourceHasRelationException;

class NewsRepository {

    private $validator;
    private $languages;

    public function __construct(NewsValidator $validator, LanguageRepository $languages)
    {
        $this->validator = $validator;
        $this->languages = $languages;
    }

    /**
     * Get all news articles
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return News::with('publishedTranslated')->whereNull('canonical_id')->get();
    }

    /**
     * Find a news entry by id
     * @param $id
     * @return News|null
     */
    public function findById($id)
    {
        return News::find($id);
    }

    /**
     * Get an option array for choosing the page in link function
     * @param string $localizeField
     * @return array
     */
    public function localizedOptionList($localizeField = null)
    {
        $options = [];

        $languages = $this->languages->visible();

        foreach ($languages as $language)
        {
            $options[$language->id] = [];
        }

        foreach (News::orderBy('title')->get() as $entry)
        {
            if ( ! $entry->title) continue;

            $text = $entry->title;

            if ( ! $entry->isPublished()) $text .= ' (Skjult)';

            $options[$entry->language_id][$entry->id] = $text;
        }

        if ($localizeField and ! empty($options))
        {
            foreach (array_keys($options) as $key)
            {
                $keys[] = $languages->find($key)->{$localizeField};
            }

            $options = array_combine($keys, $options);
        }

        return $options;
    }

    /*
    |--------------------------------------------------------------------------
    | Frontend methods
    |--------------------------------------------------------------------------
    */

    /**
     * Find a published story by it's slug
     * @param string $slug
     * @return News|null
     */
    public function findPublishedBySlug($slug)
    {
        return News::wherePublished()->whereLocale(App::getLocale())->where('slug', $slug)->first(['news.*']);
    }

    /**
     * Get recent published news entries
     * @param int $amount
     * @return Collection
     */
    public function getRecentPublished($amount = 4)
    {
        return News::wherePublished()->whereLocale(App::getLocale())->orderBy('created_at', 'desc')
            ->take($amount)->get(['news.*']);
    }

    /**
     * Get all published news
     * @return Collection
     */
    public function getPublished()
    {
        return News::wherePublished()->whereLocale(App::getLocale())->orderBy('created_at', 'desc')->get(['news.*']);
    }

    /**
     * Get an array of a menu items localized siblings
     * @param int $menuId
     * @return array
     */
    public function getLocalizedNewsItems($menuId)
    {
        $menu = News::findOrFail($menuId);

        $canonicalId = $menu->canonical_id ? $menu->canonical_id : $menu->id;

        $menus = News::whereNested(function($query) use ($canonicalId)
        {
            $query->where('news.id', $canonicalId);
            $query->orWhere('canonical_id', $canonicalId);
        })
            ->where('news.id', '!=', $menuId)
            ->wherePublished()
            ->get();

        $grouped = [];

        foreach ($menus as $menu)
        {
            $grouped[$menu->language->locale] = $menu;
        }

        return $grouped;
    }

    /*
    |--------------------------------------------------------------------------
    | CRUD
    |--------------------------------------------------------------------------
    */

    /**
     * Validate the input
     * @param $languages
     * @param array $input
     * @return array
     */
    public function validate($languages, $input)
    {
        $errors = [];

        foreach ($languages as $lang)
        {
            if (array_get($input, $lang->code.'.public'))
            {
                $v = $this->validator->create($input[$lang->code]);

                if ($v->fails()) $errors[$lang->code] = $v->getMessageBag();
            }
        }

        return $errors;
    }

    /**
     * Create a new news entry
     * @param $languages
     * @param array $input
     * @param int $userId
     * @return int
     */
    public function create($languages, $input, $userId)
    {
        $canonical = null;

        foreach ($languages as $id => $lang)
        {
            $attributes = $input[$lang->code];

            if ( ! array_get($attributes, 'publish_on')) $attributes['publish'] = null;
            if ( ! array_get($attributes, 'unpublish_on')) $attributes['unpublish'] = null;

            $page = new News($attributes);
            $page->user_id = $userId;
            $page->canonical_id = $canonical;
            $page->language_id = $lang->id;
            $page->setSlugged('url_alias', $attributes['title'], $lang->id);
            $page->save();

            if ($id == 0) $canonical = $page->id;
        }

        return $canonical;
    }

    /**
     * Update a news story
     * @param int $id
     * @param $languages
     * @param array $input
     */
    public function update($id, $languages, $input)
    {
        foreach ($languages as $lang)
        {
            $attributes = $input[$lang->code];

            $page = News::whereNested(function($query) use ($id)
            {
                $query->where('id', $id)->orWhere('canonical_id', $id);
            })->where('language_id', $lang->id)->first();

            if ( ! array_get($attributes, 'publish_on'))
            {
                $attributes['publish_on'] = 0;
                $attributes['publish'] = null;
            }

            if ( ! array_get($attributes, 'unpublish_on'))
            {
                $attributes['unpublish_on'] = 0;
                $attributes['unpublish'] = null;
            }

            // Create if not exists
            if (is_null($page))
            {
                $page = new News($attributes);
                $page->language_id = $lang->id;
                $page->canonical_id = $id;
                $page->setSlugged('url_alias', $attributes['title'], $lang->id);
                $page->save();
            }
            else
            {
                $page->setSlugged('url_alias', $attributes['title'], $lang->id);
                $page->update($attributes);
            }
        }
    }

    /**
     * Delete a news entry
     * @param int $id
     * @throws \Kameli\Foundation\Exceptions\ResourceHasRelationException
     */
    public function delete($id)
    {
        $story = News::findOrFail($id);

        $newsIds = array_merge($story->translated->lists('id'), [$story->id]);

        $links = Link::with('page')->where('function_id', 2)->whereIn('news_id', $newsIds)->get();

        if ( ! $links->isEmpty())
        {
            $e = new ResourceHasRelationException;
            $e->setRelated($links);

            throw $e;
        }

        foreach ($story->translated as $translated) $translated->delete();

        $story->delete();
    }
} 