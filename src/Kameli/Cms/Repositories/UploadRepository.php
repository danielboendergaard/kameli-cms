<?php namespace Kameli\Cms\Repositories;

use Config;
use Illuminate\Database\Eloquent\Collection;
use Intervention\Image\Image;
use Kameli\Cms\Models\Media;
use Kameli\Cms\Models\Upload;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadRepository {

    protected $whitelist = ['pdf', 'zip', 'rar', 'doc', 'docx', 'xls', 'xlsx'];

    /**
     * Find upload by id
     * @param int $id
     * @return Upload
     */
    public function findById($id)
    {
        return Upload::findOrFail($id);
    }

    /**
     * Find uploads by path
     * @param string $path
     * @return Collection
     */
    public function findByPath($path = '')
    {
        return Upload::where('path', $path)->get();
    }

    /**
     * Delete an uploaded file (soft delete)
     * @param int $id
     */
    public function delete($id)
    {
        Upload::find($id)->delete();
    }

    /**
     * Get a list of directories in the path
     * @param string $path
     * @return array
     */
    public function getDirectories($path)
    {
        $finder = (new Finder())->in(public_path('uploads/'. $path))->depth(0)->sortByName()->directories();

        $directories = [];

        foreach ($finder as $dir)
        {
            $directories[] = $dir->getRelativePathname();
        }

        return $directories;
    }

    /**
     * Create a new directory
     * @param string $name
     * @param string $path
     */
    public function createDirectory($name, $path)
    {
        @mkdir(public_path('uploads/'.$path.'/'.$name));
    }

    /**
     * Store uploaded images
     * @param UploadedFile[] $files
     * @param string $relativePath
     * @return bool
     */
    public function storeUploads($files, $relativePath)
    {
        foreach ($files as $file)
        {
            if ( is_null($file) or ! in_array($file->guessExtension(), $this->whitelist))
            {
                return false;
            }

            $filename = $file->getClientOriginalName();

            $file->move($this->getAbsolutePath($relativePath), $filename);

            Upload::create([
                'filename' => $filename,
                'path' => $relativePath
            ]);
        }
    }

    /**
     * @param UploadedFile $image
     * @param array $relativePath
     */
    protected function saveImage($image, $relativePath)
    {
        $filename = $image->getClientOriginalName();

        (new Image($image->getRealPath()))
            ->resize(1280, 720, true, false)
            ->save($this->getAbsolutePath($relativePath, 'large', $filename));

        (new Image($image->getRealPath()))
            ->resize(Config::get('kameli/cms::page_image_width'), null, true)
            ->save($this->getAbsolutePath($relativePath, 'page', $filename));

        (new Image($image->getRealPath()))
            ->resize(100, 100, true)
            ->save($this->getAbsolutePath($relativePath, 'tn', $filename));

        // Save original image
        $image->move($this->getAbsolutePath($relativePath, 'orig'), $filename);

        Media::create([
            'filename' => $filename,
            'path' => $relativePath
        ]);
    }

    /**
     * Get the path absolute path
     * @param array $relativePath
     * @param string $filename
     * @return string
     */
    protected function getAbsolutePath($relativePath, $filename = null)
    {
        return public_path('uploads/'.$relativePath) . ($filename ? '/'.$filename : '');
    }
}