<?php namespace Kameli\Cms\Repositories;

use Illuminate\Database\Eloquent\Collection;
use KameliCms;

class PermissionRepository {

    /**
     * Get all permissions
     * @return Collection
     */
    public function all()
    {
        return KameliCms::getPermissions();
    }
} 