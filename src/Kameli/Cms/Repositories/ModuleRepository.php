<?php namespace Kameli\Cms\Repositories;

use Kameli\Cms\Exceptions\ModuleRegisteredException;

class ModuleRepository {

    /**
     * @var array
     */
    protected $modules = [];

    /**
     * @var array
     */
    protected $titles = [];

    /**
     * Register a module route
     * @param string $index
     * @param string $route
     * @param string $name
     * @param bool $withParameters
     * @throws \Kameli\Cms\Exceptions\ModuleRegisteredException
     */
    public function registerRoute($index, $route, $name, $withParameters = false)
    {
        if (isset($this->modules[$index])) throw new ModuleRegisteredException("Module $index is already registered");

        $this->titles[$index] = $name;

        if ($withParameters)
        {
            $this->modules[$index] = function($parameters = []) use ($route)
            {
                return route($route, $parameters);
            };
        }
        else
        {
            $this->modules[$index] = function() use ($route)
            {
                return route($route);
            };
        }
    }

    /**
     * Register a module route with parameters
     * @param string $index
     * @param string $route
     * @param string $name
     * @throws \Kameli\Cms\Exceptions\ModuleRegisteredException
     */
    public function registerRouteWithParameters($index, $route, $name)
    {
        $this->registerRoute($index, $route, $name, true);
    }

    /**
     * Get the list of modules
     * @return array
     */
    public function getModules()
    {
        asort($this->titles);

        return $this->titles;
    }

    /**
     * Get the route for the module
     * @param string $index
     * @param array $parameters
     * @return string mixed
     */
    public function getRoute($index, $parameters = [])
    {
        return $this->modules[$index]($parameters);
    }

    /**
     * Check if module is registered
     * @param string $index
     * @return bool
     */
    public function has($index)
    {
        return isset($this->modules[$index]);
    }
}