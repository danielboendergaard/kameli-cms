<?php namespace Kameli\Cms\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Kameli\Cms\Models\Link;
use Kameli\Cms\Models\Menu;
use Kameli\Cms\Models\Page;
use Kameli\Cms\Services\Validators\MenuValidator;
use KameliCms;
use Lang;
use Str;

class MenuRepository {

    private $functions = [
        1 => 'Side',
        2 => 'Nyhed',
        3 => 'Ekstern link',
        4 => 'Modul'
    ];

    private $validator;
    private $languages;

    protected $cachedTopMenu;

    public function __construct(MenuValidator $validator, LanguageRepository $languages)
    {
        $this->validator = $validator;
        $this->languages = $languages;
    }

    /**
     * Get the menu functions
     * @return array
     */
    public function functions()
    {
        return $this->functions;
    }

    /**
     * Get all menus
     * @return Collection
     */
    public function all()
    {
        return Menu::sort()->get();
    }

    /**
     * Find a menu by id
     * @param int $id
     * @return Menu|null
     */
    public function findById($id)
    {
        return Menu::find($id);
    }

    /**
     * Get menus by an array of ids
     * @param array $ids
     * @return Collection
     */
    public function findByIds($ids)
    {
        return Menu::whereIn('id', $ids)->get();
    }

    /**
     * Get menus by an array of ids
     * @param array $ids
     * @return Collection
     */
    public function findByIdsWithTranslated($ids)
    {
        return Menu::with('translated')->whereIn('id', $ids)->get();
    }

    /**
     * Find a menu by the slug
     * @param string $slug
     * @param string $locale
     * @return Menu
     */
    public function findBySlugAndLocaleOrFail($slug, $locale)
    {
        return Menu::whereLocale($locale)->where('url_alias', $slug)->firstOrFail();
    }

    /**
     * Find a menu item from id and localee
     * @param int $id
     * @param string $locale
     * @return Menu
     */
    public function findByIdAndLocale($id, $locale)
    {
        return Menu::whereLocale($locale)
            ->whereTable('id', $id)->firstOrFail();
    }

    /**
     * @param Page $page
     * @param string $locale
     * @return Menu
     */
    public function findWithLinkToPage($page, $locale)
    {
        return Menu::whereHas('link', function($q) use ($page)
        {
            $q->where('page_id', $page->id);
            $q->where('function_id', Link::PAGE);
        })->whereLocale($locale)->where('menus.active', 1)->orderBy('views', 'desc')->first();
    }

    /**
     * @param string $locale
     * @return mixed
     */
    public function findLinkingToNews($locale)
    {
        return Menu::whereHas('link', function($q)
        {
            $q->where('function_id', Link::MODULE);
            $q->where('module', 'news');
        })->whereLocale($locale)->where('menus.active', 1)->orderBy('views', 'desc')->first();
    }

    /**
     * @param string $locale
     * @return mixed
     */
    public function findLinkingToGalleries($locale)
    {
        return Menu::whereHas('link', function($q)
        {
            $q->where('function_id', Link::MODULE);
            $q->where('module', 'galleries');
        })->whereLocale($locale)->where('menus.active', 1)->orderBy('views', 'desc')->first();
    }

    /**
     * Get the canonical page
     * @param int $pageId
     * @return Menu
     */
    public function getCanonical($pageId)
    {
        return null; // Menu::where('page_id', $pageId)->where('function_id', 1)->orderBy('views', 'desc')->first();
    }

    /**
     * Get an array of a menu items localized siblings
     * @param int $menuId
     * @return array
     */
    public function getLocalizedMenuItems($menuId)
    {
        $menu = Menu::findOrFail($menuId);

        $canonicalId = $menu->canonical_id ? $menu->canonical_id : $menu->id;

        $menus = Menu::whereNested(function($query) use ($canonicalId)
        {
            $query->where('id', $canonicalId);
            $query->orWhere('canonical_id', $canonicalId);
        })
            ->where('id', '!=', $menuId)
            ->where('active', 1)
            ->get();

        $grouped = [];

        foreach ($menus as $menu)
        {
            $grouped[$menu->language->locale] = $menu;
        }

        return $grouped;
    }

    /**
     * Increment the view count of the menu item
     * @param $menuId
     */
    public function incrementViewCount($menuId)
    {
        Menu::where('id', $menuId)->increment('views');
    }

    /**
     * @param array $languages
     * @return array
     */
    public function optionList($languages)
    {
        $options = [];

        foreach ($languages as $language)
        {
            $options[$language->id] = ['' => 'Topmenu'];
        }

        foreach (Menu::all() as $menu)
        {
            $options[$menu->language_id][$menu->id] = $menu->name;
        }

        return $options;
    }

    /**
     * Get a list of possible parent menu items
     * @return array
     */
    public function parentMenuOptionList()
    {
        $items = $this->topMenu()->lists('name', 'id');

        return ['' => 'Ingen'] + $items;
    }

    /**
     * Get the top level menu for the admin panel
     * @return Collection
     */
    public function topLevelMenu()
    {
        return Menu::with('subMenus')
            ->sort()
            ->whereNull('parent_menu_id')
            ->whereLocale(KameliCms::getDefaultLanguage())
            ->get();
    }

    /**
     * Get the top menu
     * @return Collection
     */
    public function topMenu()
    {
        if ( ! is_null($this->cachedTopMenu)) return $this->cachedTopMenu;

        return $this->cachedTopMenu = Menu::with('subMenus')
            ->sort()
            ->whereNull('parent_menu_id')
            ->where('menus.active', 1)
            ->join('languages', 'languages.id', '=', 'menus.language_id')
            ->where('languages.locale', Lang::locale())->get(['menus.*']);
    }

    public function getActiveMenu()
    {
        foreach ($this->cachedTopMenu as $menu)
        {
            if ($menu->isActive()) return $menu;
        }

        return null;
    }

    public function getActiveSubMenu()
    {
        foreach ($this->topMenu() as $menu)
        {
            if ($menu->isActive()) return $menu->getActiveSubMenus();
        }

        return null;
    }

    /**
     * Get the active secon level menu from the current menu
     * @param Menu $currentMenu
     * @return \Kameli\Cms\Models\Menu
     */
    public function getActiveSecondLevelMenu($currentMenu)
    {
        $menu = $currentMenu;

        while ($menu->level > 1)
        {
            $menu = $menu->parentMenu;
        }

        return $menu->getActiveSubMenus();
    }

    /**
     * Validate the input
     * @param Collection $languages
     * @param array $input
     * @return array
     */
    public function validate($languages, $input)
    {
        $errors = [];

        foreach ($languages as $lang)
        {
            if (array_get($input, $lang->code.'.active'))
            {
                $v = $this->validator->create($input[$lang->code]);

                if ($v->fails()) $errors[$lang->code] = $v->getMessageBag();
            }
        }

        return $errors;
    }

    /**
     * Create a new menu
     * @param Collection $languages
     * @param array $attributes
     * @return int
     */
    public function create($languages, $attributes)
    {
        $canonical = null;

        $parentMenuId = array_get($attributes, 'parent_menu_id');

        if ($parentMenuId && $parentMenu = Menu::find($parentMenuId))
        {
            $level = $parentMenu->level + 1;

            $parents = $parentMenu->translated->add($parentMenu)->groupBy(function($item)
            {
                return $item->language->code;
            });
        }

        foreach ($languages as $id => $lang)
        {
            $menu = new Menu(array_only($attributes[$lang->code], ['active', 'name']));
            $menu->language_id = $lang->id;
            $menu->canonical_id = $canonical;
            $menu->parent_menu_id = isset($parents) ? $parents[$lang->code][0]->id : null;
            $menu->setUrlAlias(Str::slug($attributes[$lang->code]['name']), $lang->id);
            $menu->level = isset($level) ? $level : 1;
            $menu->save();

            $menu->updateLinkRelation($attributes[$lang->code]);

            if ($id == 0) $canonical = $menu->id;
        }

        return $canonical;
    }

    /**
     * Update the menu item
     * @param int $id
     * @param array $languages
     * @param array $input
     */
    public function update($id, $languages, $input)
    {
        foreach ($languages as $lang)
        {
            $attributes = array_only($input[$lang->code], ['name']);

            $attributes['active'] = array_get($input, $lang->code . '.active', 0);

            // Generate the alias
            $alias = Str::slug($attributes['name']);

            // Get the menu item before update to use mutators
            $item = Menu::whereNested(function($query) use ($id)
            {
                $query->where('id', $id)->orWhere('canonical_id', $id);
            })->where('language_id', $lang->id)->first();

            if (is_null($item))
            {
                $item = new Menu($attributes);
                $item->language_id = $lang->id;
                $item->canonical_id = $id;
                $item->setUrlAlias(Str::slug($attributes['name']), $lang->id);
                $item->save();
            }
            else
            {
                $item->setUrlAlias($alias, $lang->id);
                $item->update($attributes);
            }

            $item->updateLinkRelation($input[$lang->code]);
        }
    }

    /**
     * Delete a menu item
     * @param int $id
     */
    public function delete($id)
    {
        $menus = Menu::where('id', $id)->orWhere('canonical_id', $id)->get();

        foreach ($menus as $menu) $menu->delete();
    }

    /**
     * Update the position of a menu item
     * @param int|null $parentMenuId
     * @param array $menuIds
     */
    public function updateMenuPosition($parentMenuId, $menuIds)
    {
        if ( ! is_null($parentMenuId))
        {
            $parentMenu = $this->findById($parentMenuId);

            $this->createMissingLanguages($parentMenu);
        }

        $menus = $this->findByIdsWithTranslated($menuIds);

        $priorities = array_map(function($item)
        {
            return ($item + 1) * 5;
        }, array_flip($menuIds));

        foreach ($menus as $menu)
        {
            $menu->update([
                'parent_menu_id' => $parentMenuId,
                'priority' => $priorities[$menu->id],
                'level' => is_null($parentMenuId) ? 1 : $parentMenu->level + 1
            ]);

            foreach ($menu->translated as $translated)
            {
                if (is_null($parentMenuId))
                {
                    $translated->priority = $priorities[$menu->id];
                    $translated->parent_menu_id = null;
                    $translated->level = 1;
                    $translated->save();
                }
                else
                {
                    // Find the parent menu id
                    foreach ($parentMenu->translated as $parentTranslated)
                    {
                        if ($parentTranslated->language_id == $translated->language_id)
                        {
                            $translated->priority = $priorities[$menu->id];
                            $translated->parent_menu_id = $parentTranslated->id;
                            $translated->level = $parentTranslated->level + 1;
                            $translated->save();
                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Create missing menu languages
     * @param Menu $menu
     */
    protected function createMissingLanguages($menu)
    {
        $languageIds = $this->languages->visible()->lists('id');

        $localeMenuLanguageIds = array_merge([$menu->language_id], $menu->translated->lists('language_id'));

        foreach (array_diff($languageIds, $localeMenuLanguageIds) as $id)
        {
            $newmenu = new Menu;
            $newmenu->language_id = $id;
            $newmenu->canonical_id = $menu->id;
            $newmenu->save();

            $link = Link::create([]);

            $newmenu->link()->associate($link);
        }
    }
} 