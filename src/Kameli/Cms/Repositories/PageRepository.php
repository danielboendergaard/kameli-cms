<?php namespace Kameli\Cms\Repositories;

use Kameli\Cms\Models\Link;
use Kameli\Foundation\Exceptions\ResourceHasRelationException;
use Kameli\Foundation\Exceptions\ResourceOwnershipException;
use Str;
use Kameli\Cms\Models\Page;
use Illuminate\Database\Eloquent\Collection;
use Kameli\Cms\Services\Validators\PageValidator;

class PageRepository {

    private $validator;
    private $languages;

    public function __construct(PageValidator $validator, LanguageRepository $languages)
    {
        $this->validator = $validator;
        $this->languages = $languages;
    }

    /**
     * Get all pages in default language
     * @return Collection
     */
    public function all()
    {
        return Page::with(['category', 'publishedTranslated'])->whereNull('canonical_id')->get();
    }

    /**
     * Find a page by id
     * @param int $id
     * @return Page|null
     */
    public function findById($id)
    {
        return Page::find($id);
    }

    /**
     * Find a published page by url slug, throw error if not found
     * @param string $slug
     * @param string $locale
     * @return Page
     */
    public function findPublishedBySlugAndLocale($slug, $locale)
    {
        return Page::where('url_alias', $slug)
            ->where('published', 1)
            ->whereLocale($locale)
            ->firstOrFail();
    }

    /**
     * @param array $languages
     * @return array
     */
    public function optionList($languages)
    {
        $options = [];

        foreach ($languages as $language)
        {
            $options[$language->id] = [];
        }

        foreach (Page::all() as $page)
        {
            $options[$page->language_id][$page->id] = $page->title;
        }

        return $options;
    }

    /**
     * Get an option array for choosing the page in link function
     * @param string $localizeField
     * @return array
     */
    public function localizedOptionListWithHiddenInfo($localizeField = null)
    {
        $options = [];

        $languages = $this->languages->visible();

        foreach ($languages as $language)
        {
            $options[$language->id] = [];
        }

        foreach (Page::orderBy('title')->get() as $page)
        {
            if ( ! $page->title) continue;

            $text = $page->title;

            if ( ! $page->published) $text .= ' (Skjult)';

            $options[$page->language_id][$page->id] = $text;
        }

        if ($localizeField and count($options))
        {
            foreach (array_keys($options) as $key)
            {
                $keys[] = $languages->find($key)->{$localizeField};
            }

            $options = array_combine($keys, $options);
        }

        return $options;
    }

    /**
     * Validate the input
     * @param Collection $languages
     * @param array $input
     * @return array
     */
    public function validate($languages, $input)
    {
        $errors = [];

        foreach ($languages as $lang)
        {
            if (array_get($input, $lang->code.'.published'))
            {
                $langInput = array_merge($input[$lang->code], array_only($input, ['template', 'left_content_type']));

                $v = $this->validator->create($langInput);

                if ($v->fails()) $errors[$lang->code] = $v->getMessageBag();
            }
        }

        return $errors;
    }

    /**
     * Create a new page
     * @param Collection $languages
     * @param array $input
     * @param int $userId
     * @return int
     */
    public function create($languages, $input, $userId)
    {
        $canonical = null;

        foreach ($languages as $id => $lang)
        {
            $attributes = $input[$lang->code];

            if ($attributes['template'] = $input['template'])
            {
                $attributes['main_content_type'] = $input['left_content_type'];
                $attributes['right_content_type'] = $input['right_content_type'];
            }

            $leftImages = (array) array_pull($attributes, 'images_left', []);
            $rightImages = (array) array_pull($attributes, 'images_right', []);

            $page = new Page($attributes);
            $page->user_id = $userId;
            $page->canonical_id = $canonical;
            $page->language_id = $lang->id;
            $page->setUrlAlias(Str::slug($attributes['title']), $lang->id);
            $page->save();

            if ($leftImages) $page->images()->attach($leftImages, ['position' => 'left']);
            if ($rightImages) $page->images()->attach($rightImages, ['position' => 'right']);

            if ($id == 0) $canonical = $page->id;
        }

        return $canonical;
    }

    /**
     * Update a page
     * @param int $id
     * @param Collection $languages
     * @param array $input
     */
    public function update($id, $languages, $input)
    {
        foreach ($languages as $lang)
        {
            $attributes = $input[$lang->code];

            if ($attributes['template'] = $input['template'])
            {
                $attributes['main_content_type'] = $input['left_content_type'];
                $attributes['right_content_type'] = $input['right_content_type'];
            }

            $page = Page::whereNested(function($query) use ($id)
            {
                $query->where('id', $id)->orWhere('canonical_id', $id);
            })->where('language_id', $lang->id)->first();

            // Pull image information before updating page
            $leftImages = (array) array_pull($attributes, 'images_left', []);
            $rightImages = (array) array_pull($attributes, 'images_right', []);

            // Create if not exists
            if (is_null($page))
            {
                $page = new Page($attributes);
                $page->language_id = $lang->id;
                $page->canonical_id = $id;
                $page->setUrlAlias(Str::slug($attributes['title']), $lang->id);
                $page->save();
            }
            else
            {
                $page->setUrlAlias(Str::slug($attributes['title']), $lang->id);
                $page->update($attributes);
            }

            // Detach all earlier images
            $page->images()->detach();

            if ($leftImages)
            {
                $page->images()->attach($leftImages, ['position' => 'left']);
            }

            if ($rightImages)
            {
                $page->images()->attach($rightImages, ['position' => 'right']);
            }
        }
    }

    /**
     * Delete a page
     * @param int $id
     * @throws \Kameli\Foundation\Exceptions\ResourceHasRelationException
     */
    public function delete($id)
    {
        $page = Page::findOrFail($id);

        $pageIds = array_merge($page->translated->lists('id'), [$page->id]);

        $links = Link::with('page')->where('function_id', 1)->whereIn('page_id', $pageIds)->get();

        if ( ! $links->isEmpty())
        {
            $e = new ResourceHasRelationException;
            $e->setRelated($links);

            throw $e;
        }

        foreach ($page->translated as $tPage) $tPage->delete();

        $page->delete();
    }
}