<?php namespace Kameli\Cms\Repositories;

use Config;
use File;
use Illuminate\Database\Eloquent\Collection;
use Intervention\Image\Image;
use Kameli\Cms\Models\Media;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MediaRepository {

    /**
     * Check if a media object exists
     * @param int $id
     * @return bool
     */
    public function exists($id)
    {
        return (bool) Media::where('id', $id)->count();
    }

    /**
     * Find media by id
     * @param int $id
     * @return Media|null
     */
    public function findById($id)
    {
        return Media::find($id);
    }

    /**
     * Find media by path
     * @param string $path
     * @return Collection
     */
    public function findByPath($path = '')
    {
        return Media::where('path', $path)->get();
    }

    /**
     * Delete a media file (soft delete)
     * @param int $id
     */
    public function delete($id)
    {
        Media::find($id)->delete();
    }

    /**
     * Get a list of directories in the path
     * @param string $path
     * @return array
     */
    public function getDirectories($path)
    {
        $finder = (new Finder())->in(public_path('media/orig/'. $path))->depth(0)->sortByName()->directories();

        $directories = [];

        foreach ($finder as $dir)
        {
            $directories[] = $dir->getRelativePathname();
        }

        return $directories;
    }

    /**
     * Create a new directory
     * @param string $name
     * @param string $path
     */
    public function createDirectory($name, $path)
    {
        @mkdir(public_path('media/orig/'.$path.'/'.$name));
        @mkdir(public_path('media/large/'.$path.'/'.$name));
        @mkdir(public_path('media/page/'.$path.'/'.$name));
        @mkdir(public_path('media/tn/'.$path.'/'.$name));
    }

    /**
     * Store uploaded images
     * @param UploadedFile[] $files
     * @param string $relativePath
     * @return bool
     */
    public function storeFiles($files, $relativePath)
    {
        foreach ($files as $file)
        {
            if (is_null($file) || in_array($file->guessExtension(), ['php', 'php5', 'js']))
            {
                return false;
            }

            $filename = $file->getClientOriginalName();

            if (strpos($filename, '.') === false) return false;

            $filename = substr($filename, 0, strrpos($filename, '.')) . '-' . uniqid() . substr($filename, strrpos($filename, '.'));

            if (in_array($file->guessExtension(), ['jpg', 'jpeg', 'gif', 'png']))
            {
                $this->saveImage($file, $relativePath, $filename);
            }
            else
            {
                $file->move(public_path('media/orig/'.$relativePath), $filename);

                Media::create([
                    'filename' => $filename,
                    'path' => $relativePath
                ]);
            }
        }
    }

    /**
     * @param UploadedFile $image
     * @param array $relativePath
     * @param string $filename
     */
    protected function saveImage($image, $relativePath, $filename)
    {
        (new Image($image->getRealPath()))
            ->resize(1280, 720, true, false)
            ->save($this->getAbsolutePath($relativePath, 'large', $filename));

        (new Image($image->getRealPath()))
            ->resize(Config::get('kameli/cms::page_image_width'), null, true)
            ->save($this->getAbsolutePath($relativePath, 'page', $filename));

        (new Image($image->getRealPath()))
            ->resize(100, 100, true)
            ->save($this->getAbsolutePath($relativePath, 'tn', $filename));

        // Save original image
        $image->move($this->getAbsolutePath($relativePath, 'orig'), $filename);

        Media::create([
            'filename' => $filename,
            'path' => $relativePath
        ]);
    }

    /**
     * Get the path absolute path
     * @param array $relativePath
     * @param string $size
     * @param string $filename
     * @return string
     */
    protected function getAbsolutePath($relativePath, $size, $filename = null)
    {
        return public_path('media/'.$size.'/'.$relativePath) . ($filename ? '/'.$filename : '');
    }
}