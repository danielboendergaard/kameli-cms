<?php namespace Kameli\Cms\Menu;

use ArrayIterator;
use IteratorAggregate;

class Menu implements IteratorAggregate {

    /**
     * @var Section[]
     */
    protected $sections = [];

    /**
     * Get a section by name
     * @param string $name
     * @return Section
     */
    public function getSection($name)
    {
        return $this->sections[$name];
    }

    /**
     * Add a section to the menu
     * @param string $index
     * @param Section $section
     */
    public function addSection($index, Section $section)
    {
        $this->sections[$index] = $section;
    }

    /**
     * Retrieve an external iterator
     * @return ArrayIterator;
     */
    public function getIterator()
    {
        return new ArrayIterator($this->sections);
    }
}