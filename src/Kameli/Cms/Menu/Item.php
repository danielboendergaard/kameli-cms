<?php namespace Kameli\Cms\Menu;

use HTML;
use Request;

class Item {

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $icon;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * @param string $title
     * @param string $url
     * @param string $icon
     * @param array $attributes
     */
    public function __construct($title, $url, $icon = '', $attributes = [])
    {
        $this->title = $title;
        $this->url = $url;
        $this->icon = $icon;
        $this->attributes = $attributes;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getAttributes()
    {
        return HTML::attributes($this->attributes);
    }

    /**
     * @return string
     */
    public function getActiveClass()
    {
        $active = $this->url == Request::url();

        return $active ? 'active' : '';
    }
}