<?php namespace Kameli\Cms\Menu;

use ArrayIterator;
use IteratorAggregate;

class Section implements IteratorAggregate {

    /**
     * @var string
     */
    protected $title;

    /**
     * @var Item[]
     */
    protected $items = [];

    /**
     * Create a new section
     * @param string $title
     */
    public function __construct($title)
    {
        $this->title = $title;
    }

    /**
     * Get the section title
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the items of the section
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Add an item to the section
     * @param int|null $index
     * @param string $title
     * @param string $url
     * @param string $icon
     * @param array $attributes
     */
    public function addItem($index, $title, $url, $icon = '', $attributes = [])
    {
        $this->items[$index] = new Item($title, $url, $icon, $attributes);
    }

    /**
     * Retrieve an external iterator
     * @returns ArrayIterator
     */
    public function getIterator()
    {
        // Sort the items by key
        ksort($this->items);

        return new ArrayIterator($this->items);
    }
}