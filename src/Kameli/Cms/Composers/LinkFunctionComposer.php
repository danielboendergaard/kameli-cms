<?php namespace Kameli\Cms\Composers;

use Kameli\Cms\Repositories\MenuRepository;
use Kameli\Cms\Repositories\ModuleRepository;
use Kameli\Cms\Repositories\NewsRepository;
use Kameli\Cms\Repositories\PageRepository;

class LinkFunctionComposer {

    private $menus;
    private $pages;
    private $modules;
    private $news;

    public function __construct(MenuRepository $menus,
                                PageRepository $pages,
                                NewsRepository $news,
                                ModuleRepository $modules)
    {
        $this->menus = $menus;
        $this->pages = $pages;
        $this->news = $news;
        $this->modules = $modules;
    }

    public function compose($view)
    {
        $view->_functions = $this->menus->functions();
        $view->_pages = $this->pages->localizedOptionListWithHiddenInfo('name_da');
        $view->_news = $this->news->localizedOptionList('name_da');
        $view->_modules = $this->modules->getModules();
    }
}