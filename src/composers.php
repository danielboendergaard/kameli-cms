<?php

View::composer('kameli/cms::layout.base', function($view)
{
    $view->_name = KameliCms::getWebsiteName();
    $view->_user = CmsSentry::getUser();
    $view->_menu = App::make('cms.backend.menu');
    $view->_mainCss = asset('packages/kameli/cms/css/backend.css');
});

View::composer('kameli/cms::layout.auth', function($view)
{
    $view->_name = KameliCms::getWebsiteName();
});

View::composer('kameli/cms::_partials.tracking', function($view)
{
    $view->_trackingType = KameliCms::getTrackingType();
    $view->_trackingId = KameliCms::getTrackingId();
});

View::composer('kameli/cms::_partials.link-function', 'Kameli\Cms\Composers\LinkFunctionComposer');