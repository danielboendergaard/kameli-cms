<?php

App::before(function($request)
{
    if ($request->segment(1) != Config::get('kameli/cms::backend_prefix') and ! KameliCms::isOnline())
    {
        return Response::make(KameliCms::getOfflineMessage(), 503);
    }

    return null;
});

Route::filter('auth.cms', function()
{
    if ( ! CmsSentry::check()) return Redirect::guest(route('cms.login'));

    return null;
});